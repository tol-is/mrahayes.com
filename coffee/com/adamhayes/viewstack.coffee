###
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
###

class com.adamhayes.ViewStack extends com.apo.kore.view.KoreViewStack

	el 			: '#main'

	defaultId 	: "main"

	lastScroll 	: 0

	
	#
	# Override initialize not to call super
	# super initialize also calls render but we don't need that either
	#
	initialize: ->
	
		@currentId = @defaultId


	#
	# Set Active View
	#
	setActiveView: (id=@defaultId, param=null)->
		
		# if we are moving out of main
		if @currentId is @defaultId
			# hold the scroll position
			@lastScroll = Support.scroll.scrollTop()

		# call super
		super id, param

		# set view to height
		@setViewToCurrentHeight()
		
		# if its main scroll to last pos or scroll to top
		@scrollTo if id is @defaultId then @lastScroll else 0

	
	#
	# Resize
	#
	#
	onResize:(e)->
		super e
		
		@setViewToCurrentHeight()
	

	#
	#
	#
	setViewToCurrentHeight:()->
		TweenLite.to @$el,1,
			css:
				height: @views[@currentId].getMeasuredHeight()
			ease: Expo.easeInOut


	#
	#
	#
	scrollTo:(scrollPos=0)->
		TweenLite.to window, 1,
			scrollTo:
				y: scrollPos
			ease: Expo.easeInOut
