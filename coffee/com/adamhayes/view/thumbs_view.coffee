###
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
###

class com.adamhayes.view.ThumbsView extends com.apo.kore.view.KoreView


	el 					: '#thumbs-grid-view'

	$thumbsContainer 	: null

	thumbs 				: Array()

	loadedThumbs 		: 0

	defaultColumnWidth 	: 250
	
	currentColumnWidth 	: 250

	thumbsYPos 			: 0

	active	 			: false


	#
	# Render 
	#
	render: ->
		
		@loadedThumbs = 0

		# jquery object of thumbs container
		@$thumbsContainer = $ '#thumbs-container'
		
		@generateThumbs()
					

	#
	# Generate Project Thumbnails
	#
	generateThumbs: =>

		# create thumbs for each of the models
		_.each @collection.models, (model,order) =>

			# get the thumb size in grid rows and columns
			thumb_size = model.get("thumb").thumb_size

			# default or 1 is 1/4
			newThumbGridWidth = 1
			newThumbGridHeight = 1	

			# 1 is 2/2
			if thumb_size is "2"
				newThumbGridWidth = 1
				newThumbGridHeight = 2
			# 2 is 1/1
			else if thumb_size is "3"
				newThumbGridWidth = 2
				newThumbGridHeight = 2

			# create a new ProjectThumb
			# setup initial configuration
			thumbConfig = 
				"columnWidth" 	: @currentColumnWidth 	
				"gridWidth" 	: newThumbGridWidth
				"gridHeight" 	: newThumbGridHeight

			# create a thumb instance
			newthumb = new com.adamhayes.view.ui.ProjectThumb { "model": model}, thumbConfig

			# render returns its jquery $el / append it
			@$thumbsContainer.append newthumb.$el

			# push in our dictionary
			# TODO: Maybe use a named object
			@thumbs.push newthumb


	#
	#
	#
	onScroll:(e)=>
		@loadInviewThumbs()


	#
	# On Resize Listener
	#
	onResize: (e) =>
			
		# call super to update @viewport_width and @viewport_height
		super e if e?

		# Resize $el to fit window
		@$el.width @viewport_width
		@$el.height @viewport_height

		# get exact column width
		@currentColumnWidth = @getColumnWidth @viewport_width

		# invoke resize to all children
		_.invoke @thumbs, 'resize', @currentColumnWidth

		# Relayout
		@$thumbsContainer.freetile {animate: true, elementDelay: 30}

		
	
	#
	#
	#
	loadInviewThumbs:()=>
		
		if not @active
			return @active

		_.each @thumbs, (thumb,order) =>
			 
			 fold = @viewport_height + $(window).scrollTop()
			 top = thumb.offset().top - fold

			 if fold-top >= 0 && thumb.loaded is false
			 	thumb.loadImage()
			 	@loadedThumbs++

		if @loadedThumbs is @thumbs.length
			@removeScrollListener()



	#
	# Get Column Width
	# Finds how many columns fit in a window and returns the width of a column
	#
	getColumnWidth: (width) ->

		# Updates column width based on current device
		@updateColumnWidth()

		# Calculate How many columns fit in that width
		# TODO: use different default unit per device
		columnsThatFit = Math.floor width/@defaultColumnWidth

		# Return exact column width
		return Math.floor width/columnsThatFit



	#
	# Updates Column width based on device
	#
	updateColumnWidth:->
		switch true
			when @device is "mobile"
				@defaultColumnWidth	= 150
			when @device is "tablet"
				@defaultColumnWidth = 200
			else
				@defaultColumnWidth = 250


	#
	# Get Measured Height
	#
	getMeasuredHeight :->
		@$thumbsContainer.height()+@$thumbsContainer.offset().top
		

	#
	# Hide
	#
	hide:->
		@active = false
		
		if @device is "desktop"
			@thumbsYPos = @$thumbsContainer.offset().top
			@$thumbsContainer.css "top", @thumbsYPos

		@removeScrollListener false
	
	#
	# Show
	#
	show: (catId="all") ->
		@active = true

		# set selected category
		@setSelectedCategory catId

		@addScrollListener false

		@loadInviewThumbs()

		# reset thumbs
		#@$thumbsContainer.css "top", 110
		

		
	#	
	# Set Selected Category
	#
	setSelectedCategory: (catSlug) =>
		_.invoke @thumbs, 'toggleCategorySlug', catSlug

		@dispatcher.trigger "event.selectedCategory", catSlug


