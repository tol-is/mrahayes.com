###
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
###

class com.adamhayes.view.ProjectContainerView extends com.apo.kore.view.KoreView

	el 						: '#project-view'

	addFetchedData 			: true

	isOpen 					: false

	infoOpen 				: false

	outsideViewportPos 		: 0

	direction 				: 1

	currentProjectView 		: null


	#
	# Initialize
	#
	initialize:->
		super()

		@dispatcher.on "event.infoOpen", @onInfoOpen
		@dispatcher.on "event.infoClose", @onInfoClose

		@dispatcher.on "event.nextProject", @onNextProject
		@dispatcher.on "event.prevProject", @onPrevProject
		@dispatcher.on "event.backToGrid", @onBackToGrid
		
		@dispatcher.on "event.selectedCategory", @onSelectedCategory


	#
	# Render
	#
	render:->
		@cssprop "visibility", "hidden"


	#
	# Load Project
	#
	loadProject:(projectSlug)->

		@removeCurrentProject()

		projPreview = @collection.getPreviewBySlug projectSlug

		newProject = new com.adamhayes.view.SingleProjectView projPreview, @infoOpen, @collection.currentPreviews.length
		@append newProject.$el

		@collection.fetchProjectBySlug projectSlug
		
		@currentProjectView = newProject

		@onResize()
		
		if @isOpen is false
			@activate()
			@currentProjectView.show 0
		else
			@currentProjectView.show @direction
			
	
	#
	#
	#
	onData:(e)=>
		# super trigger load complete we want to trigger when content is loaded
		# super()

		if @currentProjectView?
			projModel = @collection.getProjectModelBySlug @collection.currentProjectSlug
			
			@currentProjectView.setModel projModel

			document.title = projModel.get('title')


	#
	# Remove Current Project
	#
	removeCurrentProject:->
		if @currentProjectView?
			@currentProjectView.kill @direction*-1

	#
	#
	#
	onInfoOpen:()=>
		@infoOpen = true

	#
	#
	#
	onInfoClose:()=>
		@infoOpen = false

	#
	# On Next Project
	#
	onNextProject:(e)=>
		@direction = 1
		
		@triggerProject @collection.getNextProject e.current


	#
	# On Prev Project
	#
	onPrevProject:(e)=>
		@direction = -1

		@triggerProject @collection.getPrevProject e.current



	#
	#
	#
	addKeyListeners: ->
		
		key 'right', => 
			if @currentProjectView?
				@currentProjectView.onNextClick()

		key 'left', => 
			if @currentProjectView?
				@currentProjectView.onPrevClick()

		key 'space', => 
			if @currentProjectView?
				@currentProjectView.nextSlide()

		key 'up', => 
			if @currentProjectView?
				@currentProjectView.nextSlide()

		key 'esc, escape, del, delete', => 
			@onBackToGrid()



	#
	#
	#
	removeKeyListeners: ->
		




	#
	#
	#
	onBackToGrid:(e)=>
		@dispatcher.trigger "kore.navigate", "category/"+@collection.currentCategory


	#
	# Show
	#
	show: (projectSlug) =>
		
		@loadProject projectSlug

		return if @isOpen
		
		@isOpen = true;
		
		TweenLite.to @$el, 0.6,
			css:
				top: 0

			ease: Expo.easeOut

			onStart: ()=>
				@cssprop "visibility", "visible"


	#
	# Hide
	#
	hide: () =>
		@isOpen = false;

		TweenLite.to @$el, 0.6,
			css:
				top: @outsideViewportPos

			ease: Expo.easeInOut

			onComplete: ()=>
				@cssprop "visibility", "hidden"

		@infoOpen = false


	#
	# On Selected Category
	#
	onSelectedCategory:(category="all")=>
		@collection.filterByCategory category



	#
	# Trigger Project By Slug
	#
	triggerProject: (newprojectslug) ->
		@dispatcher.trigger "kore.navigate", "project/"+newprojectslug


	#
	# Trigger Project By Index
	#
	triggerProjectByIndex: (index) ->
		@dispatcher.trigger "kore.navigate", "project/"+@collection.getProjectSlugByIndex index



	#
	# Get Measured Height
	#
	getMeasuredHeight :->
		return @viewport_height



	#
	# On Resize Listener
	#
	onResize: (e) ->
		super e if e?

		@$el.width @viewport_width
		@$el.height @viewport_height

		@outsideViewportPos = if @device is "desktop" then @viewport_height else @viewport_height*-1
		
		if @currentProjectView?
			@currentProjectView.setSize @viewport_width, @viewport_height

		@cssprop "top", @outsideViewportPos if @isOpen is false


