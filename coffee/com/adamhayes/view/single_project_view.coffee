###
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
###

class com.adamhayes.view.SingleProjectView extends com.apo.kore.view.KoreComponent

	tag 		  	: 'div'

	infoOpen 		: false
	
	projPreview		: null

	wid 			: 0

	hei 			: 0

	allowAction 	: false

	currentLength 	: 0


  	#
	#
	#
	constructor: (@projPreview, @infoOpen, @currentLength) ->
		super()

	#
	# Initialize
	#
	initialize: () ->
		super()

		@addClass 'single-proj'	
		@cssprop 'position', 'absolute'
		@cssprop 'background-color', @projPreview.bgColor

		@enableTouch {'prevent_default': false},true

		@render()


	#
	#
	#
	setModel:(@model)->
		@renderContent()

	
	#
	#
	#
	renderContent:()->
		
		@allowAction = false
		
		@slideshow = new com.adamhayes.view.ui.ProjectSlideshow {model:@model}
		@$el.append @slideshow.$el

		@slideshow.on 'slideshow.loaded', @onSlideshowLoaded

		# draw info holder
		@drawInfoHolder()

		# draw share button
		@drawShareBtn()

		# draw info button
		@drawInfoBtn()

		if @infoOpen is true
			@onInfo()

		@setSize @width(), @height()

		@addTouchListeners()



		@addClass 'rendered'
		


	#
	#
	#
	onSlideshowLoaded:(e)=>
		#trigger complete
		@allowAction = true

		

		@dispatcher.trigger 'event.loadcomplete'

	#
	# Render
	#
	render:->
		
		@dispatcher.trigger "event.loading", @projPreview.primaryColor

		@drawArrowsAndBack()


		
	#
	#
	#
	onInfo:(e)=>
		

		@$infoBtn.toggleClass "open"

		if @$infoBtn.hasClass "open"
			@showInfo()
		else
			@closeInfo()

		

	#
	#
	#
	showInfo:()->
		
		@dispatcher.trigger 'event.infoOpen'

		# Tween cross svg
		TweenLite.to @$infoCrossSvg, 0.2,
			css:
				rotation  	: 45
				fill 		: @projPreview.copyColor

			ease: Expo.easeInOut

		# Slide in info
		TweenLite.to @$infoHolder, 0.6,
			css:
				marginLeft: 0

			ease: Expo.easeInOut
	#
	#
	#
	closeInfo:()->
		@dispatcher.trigger 'event.infoClose'

		# Tween cross svg
		TweenLite.to @$infoCrossSvg, 0.2,
			css:
				rotation 	: 0
				fill 		: @projPreview.primaryColor
			
			delay:0.3
			ease: Expo.easeInOut

		# Slide out Info
		TweenLite.to @$infoHolder, 0.6,
			css:
				marginLeft: -360

			ease: Expo.easeInOut


		
	#
	#
	#
	onNextClick: (e)=>
		#return false if @allowAction is false
		@dispatcher.trigger 'event.nextProject', {'current':@projPreview.slug}
	

	#
	#
	#
	onPrevClick: (e)=>
		#return false if @allowAction is false
		@dispatcher.trigger 'event.prevProject', {'current':@projPreview.slug}


	#
	#
	#
	onBackToGridClick :(e)=>
		#return false if @allowAction is false
		@dispatcher.trigger 'event.backToGrid'
	

	#
	# Add TouchListeners
	#
	addTouchListeners: ->
		@addListener 'swipe', @onSwipe


	removeTouchListeners: ->
		@removeListener 'swipe', @onSwipe



	#
	#
	#
	nextSlide:()=>
		if @slideshow? and @allowAction is true
			@slideshow.nextSlide()

	#
	#
	#
	prevSlide:()=>
		if @slideshow? and @allowAction is true
			@slideshow.prevSlide()

	

	#
	# On Swipe
	#
	onSwipe: (e) =>
		if e.direction is 'left'
			@onNextClick()
		else if e.direction is "right"
			@onPrevClick()
		else if e.direction is "down" and @device is 'desktop'
			@onBackToGridClick()
		else if e.direction is "up" and @device is 'mobile' or @device is 'tablet'
			@onBackToGridClick()


	#
	#
	#
	show : (direction = 1)->
		
		startpos = if direction is 0 then 0 else @width() * direction
		
		@cssprop 'marginLeft', startpos

		TweenLite.to @$el, 0.6,
			css:
				marginLeft: 0

			ease: Expo.easeInOut

	#
	#
	#
	kill : (direction = -1)->
		
		endpos = @width() * direction
		@cssprop 'marginLeft', 0

		TweenLite.to @$el, 0.6,
			#delay:0.1
			css:
				marginLeft: endpos

			ease: Expo.easeInOut
			onComplete : @remove

		if @slideshow?
			@slideshow.kill()
		
	#
	#
	#
	remove:()=>
		super()
		
		#remove default listeneres
		@$gridBtn.off 'click', @onBackToGridClick
		@$arrowNext.off 'click', @onNextClick
		@$arrowPrev.off 'click', @onPrevClick

		# if content has arrived and its rendered
		if @hasClass 'rendered'
			@removeTouchListeners()
			@slideshow.off 'slideshow.loaded', @onSlideshowLoaded
			@$infoBtn.off 'click', @onInfo

	#
	#
	#
	setSize:(width, height)=>
		@width width
		@height height

		if @slideshow?

			holderWidth = (width/12)*8
			holderHeight = if height - 130 < 900 then height - 130 else 900

			@slideshow.setSize holderWidth, holderHeight
			@slideshow.cssprop 'marginLeft', 0- holderWidth*0.5
			@slideshow.cssprop 'marginTop', 0- holderHeight*0.5


	

	# ------------------------------------------------------------
	# DRAW STUFF
	#


	#
	#
	#
	drawArrowsAndBack:()->
		# ---------------------------
		# Next Prev Arrows

		# Make the next svg arrow icon
		$nextArrowSvg = $ SVG.arrow_next
		$nextArrowSvg.css 'fill', @projPreview.primaryColor
		# The arrow button
		@$arrowNext = $ document.createElement('div')
		@$arrowNext.addClass 'arrow-next'
		@$arrowNext.append $nextArrowSvg
		@$el.append @$arrowNext
		@$arrowNext.on 'click', @onNextClick

		# Make the prev svg arrow icon
		$prevrrowSvg = $ SVG.arrow_prev
		$prevrrowSvg.css 'fill', @projPreview.primaryColor
		# The arrow button
		@$arrowPrev = $ document.createElement('div')
		@$arrowPrev.addClass 'arrow-prev'
		@$arrowPrev.append $prevrrowSvg
		@$el.append @$arrowPrev
		@$arrowPrev.on 'click', @onPrevClick

		if @currentLength is 1
			@$arrowNext.css "display", "none"
			@$arrowPrev.css "display", "none"


		# ---------------------------
		# Back to Grid

		# Make the grid svg icon
		$gridSvg = $ SVG.grid
		$gridSvg.css 'fill', @projPreview.primaryColor

		# The grid button
		@$gridBtn = $ document.createElement('div')
		@$gridBtn.addClass 'grid-btn'
		@$gridBtn.append $gridSvg
		@$gridBtn.append 'View all'
		@$gridBtn.css 'color', @projPreview.primaryColor
		@$el.append @$gridBtn
		
		#add back listener
		@$gridBtn.on 'click', @onBackToGridClick


		
		



	#
	#
	#
	drawInfoBtn:()->

		# Make the svg cross icon
		@$infoCrossSvg = $ SVG.cross
		@$infoCrossSvg.css 'fill', @projPreview.primaryColor

		#info button
		@$infoBtn = $ document.createElement('div')
		@$infoBtn.css 'color', @projPreview.primaryColor
		@$infoBtn.addClass 'info-btn'

		# append cross and Info
		@$infoBtn.append @$infoCrossSvg 
		@$infoBtn.append 'Info'
		@$el.append @$infoBtn

		#add info listener
		@$infoBtn.on 'click', @onInfo


	#
	#
	#
	drawInfoHolder:()->
		
		# Info Holder
		@$infoHolder = $ document.createElement('div')
		@$infoHolder.css 'color', @projPreview.copyColor
		@$infoHolder.css 'backgroundColor', @projPreview.primaryColor
		@$infoHolder.addClass 'info-holder'
		@$el.append @$infoHolder

		# Project title
		@$title = $ document.createElement('h2')
		@$title.addClass 'info-title'
		@$title.html @model.get('title')
		@$infoHolder.append @$title
		
		# Project description
		@$description = $ document.createElement('p')
		@$description.addClass 'info-description'
		@$description.html @model.get('info')
		@$infoHolder.append @$description

		@$infoHolder.find('a').css('color', @projPreview.copyColor)
		@$infoHolder.find('a').attr('target', '_blank')


	#
	#
	#
	drawShareBtn:()->
		
		# ---------------------------
		# Share

		# Make the share svg icon
		$shareSvg = $ SVG.share
		$shareSvg.css 'fill', @projPreview.primaryColor

		
		# The share button
		@$shareBtn = $ document.createElement('div')
		@$shareBtn.addClass 'share-btn'
		@$shareBtn.append $shareSvg
		@$shareBtn.append 'Share'
		@$shareBtn.css 'color', @projPreview.primaryColor
		@$el.append @$shareBtn


		url = Config.url+"/project/"+@projPreview.slug
		title = @model.get('title')
		pinImage = Config.url+@model.getPinImage()
		

		shareLinks = "<div class=\"share-links\"><div class=\"share-box\">"
		shareLinks += "<a href=\"http://facebook.com/sharer.php?u="+url+"&t="+title+" by Adam Hayes\" target=\"_blank\"><img src=\"/images/fb_ico.jpg\"></a>"
		shareLinks += "<a href=\"http://twitter.com/share?text="+title+" by Adam Hayes&url="+url+"\" target=\"_blank\"><img src=\"/images/twttr_ico.jpg\"></a>"
		shareLinks += "<a id=\"pntrst-link\" href=\"http://pinterest.com/pin/create/button/?url="+url+"&description="+title+" by Adam Hayes&media="+pinImage+"\" target=\"_blank\"><img src=\"/images/pntrst_ico.jpg\"></a>"
		shareLinks += "</div></div>";
		@$shareBtn.append shareLinks

		
		$pntrstlink = @$shareBtn.find("a#pntrst-link")


		if Device.isTablet or Device.isMobile
			@$shareBtn.on "click", =>

				if @slideshow?
					pinImg = @slideshow.getCurrentImage();
					$pntrstlink.attr("href", "http://pinterest.com/pin/create/button/?url="+url+"&description="+title+" by Adam Hayes&media="+pinImg)


				@$shareBtn.addClass("active")
				TweenLite.delayedCall 2, ->
					@$shareBtn.trigger("mouseout").removeClass("active")
		else
			@$shareBtn.on "mouseover", =>
				if @slideshow?
					pinImg = @slideshow.getCurrentImage();
					$pntrstlink.attr("href", "http://pinterest.com/pin/create/button/?url="+url+"&description="+title+" by Adam Hayes&media="+pinImg)
		
		
