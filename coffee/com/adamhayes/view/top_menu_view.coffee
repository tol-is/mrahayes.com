###
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
###

class com.adamhayes.view.TopMenuView extends com.apo.kore.view.KoreView

	el 				: '#top-menu-view'

	$mainmenu 		: null

	$categoriesmenu : null

	$arrow 			: null

	events  		: 
		"click #logo": "onLogoClick"
		"click .menu-item.internal": "onMenuItemClick"
		"click .category-item": "onCategoryItemClick"
	

	#
	#
	#
	initialize:()->
		if Support.device is "mobile"
			@elHeight = 65
		else
			@elHeight = 110


		super()

	#
	# Render Creates menu items
	# 
	render: =>

		#menu list holder
		@$mainmenu = $ "#main-menu"
		
		# create menu buttons
		_.each @model.get("main_menu"), (item,order) =>
			
			# set id to "main" if its the home button
			id = if item.slug is "/" then "main" else item.slug

			if item.type is "external"
				@$mainmenu.append $ "<a href=\""+item.slug+"\" class=\"menu-item\">"+item.title+"</a>"
			else
				@$mainmenu.append $ "<a href=\"/#/"+item.slug+"\" data-slug=\""+item.slug+"\" id=\""+id+"-btn\" class=\"menu-item internal\">"+item.title+"</a>"

		
		if Support.isDeadBrowser is false
			# Make the svg arrow icon
			$arrowSvg = $ SVG.arrow_menu
			$arrowSvg.css "fill", Config.copyColor

			# The arrow button
			@$arrow = $ document.createElement('div')
			@$arrow.append $arrowSvg
			@$arrow.width 12
			@$arrow.height 7
			@$arrow.css "position", "absolute"
			@$arrow.css "margin-top", 65
			@$arrow.css "pointer-events", "none"
			@$mainmenu.prepend @$arrow


		#categories menu list holder
		@$categoriesmenu = $ '#categories-menu'
		@$categoriesmenu.css "paddingLeft", 51


		@$categoriesmenu.css "background-color", Config.bgColor
		@$categoriesmenu.css "color", Config.copyColor

		# create menu buttons
		_.each @model.get("project_cats"), (item,order) =>
			@$categoriesmenu.append $ "<a href=\"/#/"+item.slug+"\" data-slug=\""+item.slug+"\" id=\""+item.slug+"-btn\" class=\"category-item "+item.slug+"\">"+item.name+"</a>"


	#
	# on Menu Item Click
	#
	onMenuItemClick: (e) =>

		# prevent default
		e.preventDefault()
		
		# get new slug
		newSlug = $(e.currentTarget).data "slug"

		# dispatch a navigate event
		@dispatcher.trigger "kore.navigate", newSlug


	#
	# on Category Item Click
	#
	onCategoryItemClick: (e) =>
		# prevent default
		e.preventDefault()
		
		# get category slug
		newSlug = $(e.currentTarget).data "slug"

		# dispatch event
		@dispatcher.trigger "kore.navigate", "category/"+newSlug

	#
	#
	#
	onLogoClick: (e) =>
		# dispatch event
		@dispatcher.trigger "kore.navigate", "/"
	
	#
	# On Route Change
	#
	routeChange: (id="main", param="all")->
				
		@setSelectedMainButton id


		if id is "main"
			@showCategoriesMenu()
			@setSelectedCategory param
		else
			@hideCategoriesMenu()


	#
	# Reset Selected Main button
	#
	resetSelectedMainButton:()->
		@$mainmenu.find('a.menu-item').removeClass("selected")


	#
	# Set Selected Main button
	#
	setSelectedMainButton : (slug="main")->
		
		if slug isnt "about"
			slug = "main"

		@resetSelectedMainButton()
		
		@$newButton = @$mainmenu.find "#"+slug+"-btn"

		@$newButton.addClass "selected"

		if @$arrow? and @$newButton?
			@moveMenuArrowTo @$newButton


	#
	# Reset Selected Category button
	#



	moveMenuArrowTo: ($btn)->
		
		if @device is "mobile"
			newPos = - 6 +$btn.offset().left + $btn.width()*0.5
		else
			newPos = -30 - 6 +$btn.offset().left + $btn.width()*0.5
		

		TweenLite.to @$arrow, 0.6,
			css:
				marginLeft:newPos

			ease: Expo.easeOut


	


	#
	# Reset Selected Category button
	#
	resetSelectedCategories:()->
		@$categoriesmenu.find('a.category-item').removeClass("selected")


	#
	# Set Selected Category button
	#
	setSelectedCategory : (catlug)->
		@resetSelectedCategories()

		newi = @$categoriesmenu.find('.'+catlug)

		@$categoriesmenu.find('.'+catlug).addClass("selected")
		

	#
	# Show Categories Menu
	#
	showCategoriesMenu: ->
		
		# el height
		TweenLite.to @$el, 0.6,
			css:
				height:@elHeight

			ease: Expo.easeOut

		# show submenu
		TweenLite.to @$categoriesmenu, 0.4,
			css:
				top:60

			ease: Expo.easeOut


	#
	# Hide Categories Menu
	#
	hideCategoriesMenu: ->
		
		# el height
		TweenLite.to @$el, 0.6,
			css:
				height:65

			ease: Expo.easeOut
		
		# hide submenu
		TweenLite.to @$categoriesmenu, 0.6,
			css:
				top:0
			delay: 0.1
			ease: Expo.easeInOut



	#
	# On Resize Listener
	#
	onResize: (e) ->
		
		if @device is "mobile"
			@elHeight = 65
		else
			@elHeight = 110

		@$el.width e.viewport_width
		@$el.height @elHeight
		
		@$mainmenu.width e.viewport_width
		@$categoriesmenu.width e.viewport_width - 51

		if @$arrow? and @$newButton?
			@moveMenuArrowTo @$newButton

	#
	# On Device Change Listener
	#
	onDeviceChange: (e) =>
		super e

		if @device is "mobile"
			#@$arrow.css "display", "none"
		else
			#@$arrow.css "display", "block"



			





