###
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
###

class com.adamhayes.view.AboutView extends com.apo.kore.view.KoreView

	el 					: '#about-view'

	$elHeight 			: 0

	isOpen 				: false

	$close 				: null

	$about 				: null

	$commissions 		: null

	$contact 			: null

	topPos 				: 65;


	#
	#
	#
	initialize:()->


		super()


	#
	# Render
	#
	render:->

		# $el properties
		@$el.css "background-color", Config.bgColor
		@$el.css "color", Config.copyColor


		# Make the svg close icon
		$closeSvg = $ SVG.close
		$closeSvg.css "fill", Config.copyColor

		# The close button
		@$close = $ document.createElement('div')
		@$close.width 12
		@$close.height 12
		@$close.append $closeSvg
		@$close.css "position", "absolute"
		@$close.css "padding", "30"
		@$close.css "right", 0
		@$close.css "cursor", "pointer"
		@$el.prepend @$close

		@$close.on "click", @onCloseClick


		# About Column
		@$about = $ '#about'
		@$about.append @model.get('main_copy')
		@$about.append "<div class=\"clearfix\"></div>"
		@$about.append "<h2>Selected Clients</h2>"
		@$about.append @model.get('selected_clients_copy')


		# Commisssions Column
		@$commissions = $ '#commissions'
		@$commissions.append "<h2>Commissions</h2>"
		@$commissions.append @model.get('commisions_copy')
		@$commissions.append "<div class=\"clearfix\"></div>"




		# Make the svg twitter icon
		$TwitterSvg = $ SVG.twitter
		$TwitterSvg.css "fill", Config.copyColor

		# The twitter ico
		$twitterIco = $ document.createElement('div')
		$twitterIco.width 21
		$twitterIco.height 17
		$twitterIco.append $TwitterSvg
		$twitterIco.css "position", "absolute"
		$twitterIco.css "marginLeft", -30

		#Twitter p for contact
		$twitter = $ document.createElement('p')
		$twitter.append $twitterIco
		$twitter.append "<a href=\""+@model.get('twitter_link')+"\" target=\"_blank\">Follow me</a>"



		# Contact Column
		@$contact = $ '#contact'
		@$contact.append "<h2>Contact</h2>"
		@$contact.append @model.get('contact_copy')
		@$contact.append $twitter
		@$contact.append "<p>" + @model.get('copyrights') + "<br><a href=\"http://enjoythis.co.uk\">Enjoythis</a> website" + "</p>"
		@$contact.append "<div class=\"clearfix\"></div>"

		@$el.find('a').css "color", Config.copyColor


	#
	# On Close Click
	#
	onCloseClick:(e)=>
		@dispatcher.trigger "kore.navigate.back"


	#
	# Show View
	#
	show: (time = 0.6) ->

		# set isOpen
		@isOpen = true;

		# Tween View
		TweenLite.to @$el, time,
			css:
				top: @topPos

			ease: Expo.easeInOut



	#
	# Hide View
	#
	hide: (time = 0.6) ->

		# set isOpen
		@isOpen = false;

		# Tween View
		TweenLite.to @$el, time,
			css:
				top: 0-@$elHeight - @topPos

			ease: Expo.easeInOut



	#
	# On Resize Listener
	#
	onResize: (e) ->
		super e

		# update layout
		@updateLayout()

		# update $el width
		@$el.width e.viewport_width

		# update elHeight param
		@$elHeight = @$el.outerHeight()+@topPos



		# if view is not open
		# move outside the viewport
		if @isOpen is false
			@cssprop "top", 0-@$elHeight
		else
			@cssprop "top", @topPos



	#
	# Updates Layout based on width
	#
	updateLayout:->

		# Find column width
		colWidth = @viewport_width/12;

		# Update Layout
		switch true

			# Less than 768
			# A landscape handheld
			when @viewport_width < 480
				@$about.width colWidth * 10
				@$about.css "marginLeft", colWidth
				@$about.css "paddingTop", colWidth+20
				@$about.css "paddingBottom", 0

				@$commissions.width colWidth * 10
				@$commissions.css "marginLeft", colWidth
				@$commissions.css "paddingTop", colWidth
				@$commissions.css "paddingBottom", 0

				@$contact.width colWidth * 10
				@$contact.css "marginLeft", colWidth
				@$contact.css "paddingTop", colWidth
				@$contact.css "paddingBottom", colWidth
				@$contact.css "paddingLeft", 0

			# Less than 1024
			# Works for horizontal tablet
			when @viewport_width < 1024
				@$about.width colWidth * 10
				@$about.css "marginLeft", colWidth
				@$about.css "paddingTop", colWidth
				@$about.css "paddingBottom", 0

				@$commissions.width colWidth * 5
				@$commissions.css "marginLeft", colWidth
				@$commissions.css "paddingTop", colWidth
				@$commissions.css "paddingBottom", colWidth

				@$contact.width colWidth * 5
				@$contact.css "marginLeft", 0
				@$contact.css "paddingTop", colWidth
				@$contact.css "paddingBottom", colWidth
				@$contact.css "paddingLeft", 35

			# Else
			# Usually a desktop
			else
				@$about.width colWidth * 5
				@$about.css "marginLeft", colWidth
				@$about.css "paddingTop", colWidth
				@$about.css "paddingBottom", colWidth

				@$commissions.width colWidth * 2
				@$commissions.css "marginLeft", colWidth
				@$commissions.css "paddingTop", colWidth
				@$commissions.css "paddingBottom", colWidth

				@$contact.width colWidth * 2
				@$contact.css "marginLeft", 0
				@$contact.css "paddingTop", colWidth
				@$contact.css "paddingBottom", colWidth
				@$contact.css "paddingLeft", 35


	#
	# Get Measured Height
	#
	getMeasuredHeight :->
		return if @$elHeight > @viewport_height then @$elHeight else @viewport_height

	#
	# On Device Change Listener
	#
	onDeviceChange: (e) =>
		super e


