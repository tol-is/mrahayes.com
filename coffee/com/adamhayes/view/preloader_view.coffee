###
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
###

class com.adamhayes.view.PreloaderView extends Backbone.View

	
	el 			: "#preloader"


	#
	# Initialize
	# Add Listeners
	#
	initialize : ->

		@hide(0)

		# add kore listeners
		@dispatcher.on "event.loading", @onLoading
		@dispatcher.on "event.loadcomplete", @onLoadComplete

		@render()


	#
	#
	#
	render:()->
		opts =
			lines: 9 # The number of lines to draw
			length: 2 # The length of each line
			width: 5 # The line thickness
			radius: 16 # The radius of the inner circle
			corners: 1 # Corner roundness (0..1)
			rotate: 0 # The rotation offset
			color: "#000" # #rgb or #rrggbb
			speed: 1 # Rounds per second
			trail: 100 # Afterglow percentage
			shadow: false # Whether to render a shadow
			hwaccel: false # Whether to use hardware acceleration
			className: "spinner" # The CSS class to assign to the spinner
			zIndex: 2e9 # The z-index (defaults to 2000000000)
			top: "auto" # Top position relative to parent in px
			left: "auto" # Left position relative to parent in px

		@spinner = new Spinner(opts).spin()
		@$el.append @spinner.el
		


	#
	# onLoading
	#
	onLoading:(param) =>
		
		@show 0.4, param


	#
	# onLoadComplete
	#
	onLoadComplete:(e) =>
		@hide 1

	#
	#
	#
	show: (time = 0, color="#cccccc") ->
		
		@$el.addClass "active"
		
		TweenLite.to @$el, time,
			css:
				autoAlpha 	: 1
			ease: Expo.easeOut

		
		@$el.find('div').css("background-color", color);


	#
	#
	#
	hide: (time = 0) =>

		
		TweenLite.to @$el, time,
			css:
				autoAlpha: 0

			ease: Expo.easeOut
			onComplete:()=>
				

				TweenLite.killTweensOf @$el
				@$el.removeClass "active"

		

	#
	#
	#
	reposition:(height)->
		@$el.css "top", height*0.5