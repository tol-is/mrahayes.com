###
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
###

class com.adamhayes.view.ui.ProjectSlideshow extends com.apo.kore.view.KoreComponent

	tag 		  		: 'div'

	slidesLoaded		: 0

	children 			: []

	currentSlidePos 	: 0

	currentSlide 		: null

	isVideoSlide 	 	: false

	cancelSlideshow 	 : false

	isDead 				: false

	#
	#
	#
	constructor:(options, @primaryColor)->
		super options


	#
	#
	#
	initialize: () ->
		super()
		
		@isDead = false

		@addClass "project-slideshow"

		@enableTouch {'prevent_default': false},true
		
		@render()

		@addTouchListeners()


	#
	#
	#
	render:()=>
		@children = []

		#add slides
		_.each @model.get("content"), (content,order) =>
			
			slide = {}

			switch content.type
				when "project_image"
					slide = new com.adamhayes.view.ui.ImageSlide {}, content					
				when "project_video"
					slide = new com.adamhayes.view.ui.VideoSlide {}, content

			slide.on "load.complete", @onSlideLoaded
			@append slide.$el
			@children.push slide

		
		# add slideshow bullets
		if @children.length>1
			@$bullets = $ document.createElement('div')
			@$bullets.addClass "slideshow-nav"
			@append @$bullets
			
			_.each @model.get("content"), (content,order) =>
				$bullet = $ document.createElement('span')
				@$bullets.append $bullet

				$bullet.addClass "bullet"
				$bullet.data "order", order

				$bulletSvg = $ SVG.bullet
				$bulletSvg.css "fill", Config.bulletColor

				$bullet.append $bulletSvg
				$bullet.on "click",@onBulletClick
				
	#
	#
	#
	onBulletClick:(e)=>
		e.preventDefault();

		$bullet = $ e.currentTarget

		@cancelSlideshow = true

		@showSlide $bullet.data "order"

		


	#
	#
	#
	onSlideLoaded:(e)=>
		@slidesLoaded++
		
		if @slidesLoaded is @model.get("content").length
			@trigger "slideshow.loaded"
			@startup()

	
	#
	#
	#
	startup:()->
		@show()

		@startSlideshow()
		
		@showSlide()
		
	
	#
	#
	#
	showSlide:(pos = 0)->
		
		# if we have a current slide hide it
		if @currentSlide?
			@currentSlide.hide()

			# if there is current slide and we have bullets reset selected bullet
			if @$bullets?
				$prevbullet = @$bullets.find(".bullet.selected")
				$prevbullet.removeClass "selected"
				$prevbullet.find("svg").css "fill", Config.bulletColor
		
		# if new position is valid
		if @children[pos]?
			# set new slide
			@currentSlide = @children[pos]
			@currentSlidePos = pos
			@currentSlide.show()

			# if we have bullets activate new bullet
			if @$bullets?
				$newbullet = @$bullets.find(".bullet").eq(pos)
				$newbullet.addClass "selected"
				$newbullet.find("svg").css "fill", @model.get "primaryColor"

			if @currentSlide.type is "VideoSlide" or @cancelSlideshow is true
				@stopSlideshow();
			else
				@resetSlideshow()


	#
	#
	#
	startSlideshow:()->
		TweenLite.delayedCall Config.slideshowTimer, @slideshowStep


	#
	#
	#
	stopSlideshow:()->
		TweenLite.killDelayedCallsTo @slideshowStep


	#
	#
	#
	resetSlideshow:()->
		@stopSlideshow()
		@startSlideshow()


	#
	#
	#
	slideshowStep:()=>
		@nextSlide()


	#
	# Add Listeners
	#
	addTouchListeners: ->
		@addListener 'tap', @onTap


	#
	#
	#
	removeTouchListeners: ->
		@removeListener 'tap', @onTap


	#
	# On Tap
	#
	onTap: (e) =>

		e.preventDefault();

		@cancelSlideshow = true

		@nextSlide()



	#
	#
	#
	nextSlide:()=>
		
		return if @children.length is 1

		if @currentSlidePos < @children.length-1
			@showSlide @currentSlidePos+1
		else
			@showSlide()



	#
	#
	#
	prevSlide:()=>

		return if @children.length is 1

		@isVideoSlide = false

		if @currentSlidePos is 0
			@showSlide @children.length-1
		else
			@showSlide @currentSlidePos-1
		

	#
	#
	#
	show:()=>
		return if @isDead is true
		
		TweenLite.to @$el, 0.4,
			css:
				autoAlpha: 1

			ease: Expo.easeOut

	#
	#
	#
	hide:()->
		if @children[@currentSlidePos]?
			@children[@currentSlidePos].hide()


	#
	#
	#
	kill:()->
		@isDead = true

		_.each @children, (slide) =>
			slide.off "load.complete", @onSlideLoaded

		@removeTouchListeners()

		#@$bullets.find(".bullet").off "click", @onBulletClick

	#
	#
	#
	setSize:(width, height)->
		super width,height

		# height is smaller if we have bullets
		slidesHeight = if @$bullets? then height-50 else height

		# if bullets reposition $bullets
		if @$bullets?
			@$bullets.css "margin-top", height-50

		# invoke resize to each image
		_.invoke @children, 'setSize', width,slidesHeight

	
	#
	#
	#
	getCurrentImage:()=>
			
		if @currentSlide? and @currentSlide.slidedata.type is "project_image"
			return Config.url+@currentSlide.slidedata.image.img_700

		return Config.url+@model.getPinImage()




