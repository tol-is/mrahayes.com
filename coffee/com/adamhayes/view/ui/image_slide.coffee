###
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
###

class com.adamhayes.view.ui.ImageSlide extends com.apo.kore.view.KoreComponent

	type 			: "ImageSlide"

	slidedata 		: null

	image 			: null

	$image 			: null

	imageRatio 		: 0

	
	#
	#
	#
	constructor:(options, @slidedata)->
		super options


	#
	# Initialize
	#
	initialize: () ->
		super()

		@addClass "project-slide"
		@addClass @slidedata.type

		# create an Image object
		@image = new Image()

		# the jQuery image object
		@$image = $ @image

		@render()


	#
	#
	#
	render:()->
		
		# calculate imageRatio used for resize
		@imageRatio = @slidedata.image.width / @slidedata.image.height

		# setup an empty image with the actual thumb's width and height
		@image.src = "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
		@image.width = @slidedata.image.width
		@image.height = @slidedata.image.height

		# append the image
		@append @image

		@loadImage()


	#
	# Load actual thumb
	#
	loadImage:() ->

		# on load event listener
		@image.onload = @loadComplete

		# TODO this should be known already
		wid = $(window).width()
		hei = $(window).height()

		# assume a default reasonable size
		imgSrc = @slidedata.image.img_700		

		#but find which image file we want per device, retina and viewport width
		switch true
			
			# tablet options
			when Device.isTablet is true and Support.isRetina is true
				imgSrc = @slidedata.image.img_900
			when Device.isTablet is true and Support.isRetina is false
				imgSrc = @slidedata.image.img_700
	
			# mobile options
			when Device.isMobile is true and Support.isRetina is true
				imgSrc = @slidedata.image.img_700
			when Device.isMobile is true and Support.isRetina is false
				imgSrc = @slidedata.image.img_400

			# desktop options
			when Device.isDesktop is true and Support.isRetina is true
				imgSrc = @slidedata.image.img_1200
			when Device.isDesktop is true and wid > 960 and hei > 1000
				imgSrc = @slidedata.image.img_1200
			when Device.isDesktop is true and wid > 960 and hei > 700
				imgSrc = @slidedata.image.img_900
			when Device.isDesktop is true
				imgSrc = @slidedata.image.img_700


		#load source
		@image.src = imgSrc

	#
	# On Image Loaded
	#
	loadComplete: () =>
		@addClass "loaded"
		@trigger "load.complete"


	#
	#
	#
	show:()->
	
		TweenLite.to @$el, 0.8,
			css:
				autoAlpha: 1

			ease: Expo.easeOut


	#
	#
	#
	hide:()->
		TweenLite.to @$el, 0.8,
			css:
				autoAlpha: 0

			ease: Expo.easeOut

	#
	#
	#
	setSize:(width,height)->

		# calculate dimensions to fit both sides
		newwid = width
		newhei = width/@imageRatio
		if newhei > height
			newhei = height
			newwid = newhei*@imageRatio

		# set dimensions
		@width newwid
		@height newhei

		#Center Slide to Container
		@cssprop "marginLeft", width*0.5 - @width()*0.5
		@cssprop "marginTop", height*0.5 - @height()*0.5


	