###
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
###

class com.adamhayes.view.ui.ProjectThumb extends com.apo.kore.view.KoreComponent

	tagName 		: "div"

	boxWidth 		: 100

	boxHeight 		: 75

	columnWidth 	: 100

	gridWidth  		: 1
	
	gridHeight  	: 1

	image 			: null

	$image 			: null

	imageRatio 		: 0

	$info 			: null

	infoPadding 	: 20

	$infoDirection  : 1

	$cross 			: null

	$hideBox 		: null

	hoverComplete 	: false

	active 			: true

	loadOnce 		: false

	loaded 			: false


	constructor: (options, config) ->
		super options

		@columnWidth = Number config.columnWidth
		@gridWidth = Number config.gridWidth
		@gridHeight = Number config.gridHeight

		
	#
	# Initialize
	#
	initialize: () ->

		@addClass "proj-thumb"	

		# TEMP: Add a random background 
		#randColor = "#"+((1<<24)*Math.random()|0).toString(16)
		#@cssprop "background-color", randColor

		@cssprop "overflow", "hidden"
		@cssprop "position", "absolute"
		@cssprop "left", "-1000px"
		@cssprop "top", "-1000px"

		# create an Image object
		@image = new Image()

		# the jQuery image object
		@$image = $ @image

		@render()


	#
	# Render 
	#
	render: () ->

		# add each category as class to enable disable
		_.each  @model.get("cats"), (cat)=>
			@addClass cat


		@addClass "proj-thumb"


		#-------------------
		# Create thumbnail

		# get the thumb json
		thumb = @model.get("thumb")

		# calculate imageRatio used for resize
		@imageRatio = thumb.width / thumb.height

		# setup an empty image with the actual thumb's width and height
		@image.src = "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
		@image.width = thumb.width
		@image.height = thumb.height

		# append the image
		@$el.append @image

		#-------------------
		# Thumb Hover

		# Info Container
		@$info = $ document.createElement('div')
		@$info.addClass "thumb-info"
		@$info.css "background-color", Config.thumbBgColor
		@$info.css "color", Config.thumbCopyColor
		@$info.css "padding", @infoPadding
		@$info.css "opacity", 0
		@$el.append @$info

		# Make the svg cross icon
		$crossSvg = $ SVG.cross
		$crossSvg.css "fill", Config.thumbCopyColor

		# The cross icon holder
		@$cross = $ document.createElement('div')
		@$cross.width 12
		@$cross.height 12
		@$cross.append $crossSvg 
		@$cross.css "pointer-events", "none"
		@$info.append @$cross

		# Thumb title
		@$title = $ document.createElement('h2')
		@$title.addClass "thumb-title"
		@$title.html @model.get("title")
		@$title.css "marginTop", @infoPadding
		@$title.css "pointer-events", "none"
		@$info.append @$title

		#-------------------
		# Thumb Disabled Box

		@$hideBox = $ document.createElement('div')
		@$hideBox.addClass "thumb-hide-box"
		@$hideBox.css "background-color", "#000000"
		@$hideBox.css "position", "absolute"	
		@$hideBox.css "opacity", 0.85
		@$el.append @$hideBox

		
		## update size to the default column width
		@resize(@columnWidth)

		# load image
		# TODO: load when is nearly inview
		#@loadImage()

		# Enable Hammer
		@enableTouch {prevent_default:false, hold_timeout:100, swipe:false, drag:false}
		

	

	#
	# Load actual thumb
	#
	loadImage : ->
		if @loadOnce is true
			return false

		@loadOnce = true

		@loaded = true

		# on load event listener
		@image.onload = @loadComplete

		# assume a default reasonable size
		if @gridWidth is 2 
			thumbSrc = @model.get("thumb").t_600
		else if @gridWidth is 1
			thumbSrc = @model.get("thumb").t_300

		#but find which image file we want per device, retina and grid width
		switch true
		
			# tablet options
			when Device.isTablet is true and @gridWidth is 2 and Support.isRetina is true
				thumbSrc = @model.get("thumb").t_600
			when Device.isTablet is true and @gridWidth is 1 and Support.isRetina is true
				thumbSrc = @model.get("thumb").t_300
			when Device.isTablet is true and @gridWidth is 2 and Support.isRetina is false
				thumbSrc = @model.get("thumb").t_600
			when Device.isTablet is true and @gridWidth is 1 and Support.isRetina is false
				thumbSrc = @model.get("thumb").t_300


			# mobile options
			when Device.isMobile is true and @gridWidth is 2 and Support.isRetina is true
				thumbSrc = @model.get("thumb").t_400
			when Device.isMobile is true and @gridWidth is 1 and Support.isRetina is true
				thumbSrc = @model.get("thumb").t_300
			when Device.isMobile is true and @gridWidth is 2 and Support.isRetina is false
				thumbSrc = @model.get("thumb").t_300
			when Device.isMobile is true and @gridWidth is 1 and Support.isRetina is false
				thumbSrc = @model.get("thumb").t_200

			# desktop options
			when Device.isDesktop is true and @gridWidth is 2
				thumbSrc = @model.get("thumb").t_600
			when Device.isDesktop is true and @gridWidth is 1
				thumbSrc = @model.get("thumb").t_400

		#load source
		@image.src = thumbSrc
		

	#
	# On Image Loaded
	#
	loadComplete: () =>
		TweenLite.to @$el, 1,
			css:
				opacity:1
			ease: Expo.easeOut

		@addClass "loaded"


	#
	# Update Thumb Size
	#
	resize: (value) ->
		
		# Get Current Container Width
		@boxWidth = Math.floor(@gridWidth*value)
		@boxHeight = Math.floor(@gridHeight*value*0.75)
		
		# Resize Element
		@width @boxWidth
		@height @boxHeight

		# Resize Image to fit width
		@$image.width @boxWidth
		@$image.height Math.floor(@boxWidth/@imageRatio)

		# If it doesn't cover the height
		# Resize to fit height
		if @$image.height() < @boxHeight
			@$image.height @boxHeight
			@$image.width Math.floor(@boxHeight*@imageRatio)

		# Center Image to Container
		@$image.css "margin-left", 0 - Math.abs((@boxWidth - @$image.width()) * 0.5 )
		@$image.css "margin-top", 0 - Math.abs((@boxHeight - @$image.height()) * 0.5 )

		# Resize Info to fit width
		@$info.width @boxWidth - @infoPadding*2
		@$info.height @boxHeight - @infoPadding*2

		# Resize Hide Box to fit width
		@$hideBox.width @boxWidth 
		@$hideBox.height @boxHeight 
		
		###
		# Info Box will be placed just outside the container in a random position
		# generate a random int
		@infoBoxDirection = randomInt 0,4

		switch @infoBoxDirection
			when 0
				marginLeft = 0
				marginTop = 0 - @boxHeight
			when 1
				marginLeft = @boxWidth 
				marginTop = 0
			when 2
				marginLeft = 0
				marginTop = @boxHeight
			when 3
				marginLeft = 0 - @boxWidth
				marginTop = 0

		# Position info outside the container
		@$info.css "margin-top", marginTop
		@$info.css "margin-left", marginLeft
		###

		# Position info outside the container
		if @active is true
			@$hideBox.css "margin-top", 0 - @boxHeight


	#
	# Override Show
	# TODO: Slide the image in
	#
	show : ->
		#super (0)
		TweenLite.to @$image, 0.4,
			css:
				autoAlpha: 1

			ease: Expo.easeIn

	#
	# Toggles is it belongs to a category
	#
	toggleCategorySlug:(slug)->
		if @hasClass slug then @activate() else @deactivate()
	
	#
	# Activate
	#
	activate: ->
		super()

		TweenLite.to @$hideBox, 0.4,
			css:
				marginTop: 0 - @boxHeight

			ease: Expo.easeInOut
	#
	# Deactivate
	#
	deactivate: ->
		super()

		TweenLite.to @$hideBox, 0.4,
			css:
				marginTop: 0

			ease 		: Expo.easeInOut
			

	#
	# Add Listeners
	#
	addTouchListeners: ->
		@addListener "tap", @onTap
		#@addListener "hold", @onHold
		#@addListener "doubletap", @onDoubleTap

	removeTouchListeners: ->
		@removeListener "tap", @onTap
		#@removeListener "hold", @onHold
		#@removeListener "doubletap", @onDoubleTap

	#
	# On Tap
	#
	onTap: (e) =>
		@onMouseClick e
		#@onMouseOver e
		#TweenLite.delayedCall 1, @onMouseOut

	
	#
	# On Double Tap
	#
	onDoubleTap: (e) =>
		@onMouseClick e
	

	#
	# On Hold
	#
	onHold: (e) =>
		@addListener "release", @onRelease
		@onMouseOver e

	#
	# On Release
	#
	onRelease: (e) =>
		@removeListener "release", @onRelease
		@onMouseOut e


	#
	# On Mouse Over
	#	
	onMouseOver: (e) =>
		TweenLite.to @$info, 0.5,
			css:
				opacity:1
				#marginTop: 0
				#marginLeft: 0

			ease: Expo.easeOut
			onComplete: ()=>
				#@hoverComplete  = true
	
	#
	# On Mouse Out
	#
	onMouseOut: (e) =>

		###
		if(@hoverComplete)
			@infoBoxDirection = randomInt 0,4

		switch @infoBoxDirection
			when 0
				marginLeft = 0
				marginTop = 0 - @boxHeight
			when 1
				marginLeft = @boxWidth 
				marginTop = 0
			when 2
				marginLeft = 0
				marginTop = @boxHeight
			when 3
				marginLeft = 0 - @boxWidth
				marginTop = 0
		###

		TweenLite.to @$info, 1,
			css:
				opacity:0
				#marginTop: marginTop
				#marginLeft: marginLeft
			delay:0
			ease: Expo.easeOut
			#ease: Expo.easeIn
			onComplete: ()=>
				#@hoverComplete = false

	#
	# On Thumb Click
	#
	onMouseClick: (e) =>
		# prevent default
		e.preventDefault()
		
		# get project slug
		newSlug = @model.get "slug"

		# dispatch a navigate event
		@dispatcher.trigger "kore.navigate", "project/"+newSlug
