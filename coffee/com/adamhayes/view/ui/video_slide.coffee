###
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
###

class com.adamhayes.view.ui.VideoSlide extends com.apo.kore.view.KoreComponent

	type 			: "VideoSlide"

	slidedata 		: null

	rendered 		: false

	framewidth  	: 0

	frameheight 	: 0

	
	#
	#
	#
	constructor:(options, @slidedata)->
		super options


	#
	# Initialize
	#
	initialize: () ->
		super()

		@addClass "project-slide"
		@addClass @slidedata.type

		TweenLite.delayedCall 0.01, @loadComplete

		# Swipe overlay
		@$swipeOverlay = $ document.createElement('div')
		@$swipeOverlay.addClass "swipeOverlay"
		@$swipeOverlay.css 'opacity', 0
		#@$swipeOverlay.css 'display', 'none'

	#
	# On Loaded
	#
	loadComplete: () =>
		@addClass "loaded"
		@trigger "load.complete"

	
	#
	#
	#
	show:()->

		@append @$swipeOverlay
		@append htmlDecode @slidedata.embed_code
		@setSize @framewidth, @frameheight

		TweenLite.to @$el, 0.4,
			css:
				autoAlpha: 1

			ease: Expo.easeOut


	#
	#
	#
	hide:()=>
		TweenLite.to @$el, 0.4,
			css:
				autoAlpha: 0

			ease: Expo.easeOut
			onComplete:@removeVideo
	
	#
	#
	#
	removeVideo:()=>
		
		iframe = @find "iframe"

		iframe.remove()
		iframe = {}

		@$swipeOverlay.remove()


	#
	#
	#
	setSize:(width,height)->
		
		@framewidth = width
		@frameheight = height

		iframe = @find "iframe"

		if iframe?

			aspect_ratio = @slidedata.aspect_ratio

			# fit both sides
			newwid = width
			newhei = width/aspect_ratio

			if newhei > height
				newhei = height
				newwid = newhei*aspect_ratio

			# apply width and height
			@width newwid
			@height newhei

			
			if @device is "mobile"
				overlayRatio = 0.65
			else
				overlayRatio = 0.75
				

			# resize swipe overlay
			if @$swipeOverlay
				@$swipeOverlay.width newwid
				@$swipeOverlay.height newhei * overlayRatio


			#Center Slide to Container
			@cssprop "marginLeft", width*0.5 - newwid*0.5
			@cssprop "marginTop", height*0.5 - newhei*0.5

			
			