###
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
###

class com.adamhayes.model.ProjectModel extends Backbone.Model


	#
	# Get Pin Image
	# 
	#
	getPinImage: () ->
		
		firstObj = _.find @get('content'), (obj) ->
			obj.type is "project_image"
		
		return if firstObj? then firstObj.image.img_700 else ""
		
