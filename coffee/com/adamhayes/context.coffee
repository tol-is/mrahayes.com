###
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
###

# Dependencies
#<< com/apo/kore/*
#<< com/adamhayes/collection/*
#<< com/adamhayes/controller/*
#<< com/adamhayes/model/*
#<< com/adamhayes/view/*


class com.adamhayes.Context extends com.apo.kore.view.KoreContext

	# backbone view dom element
	el							: 'body'

	# Kore param to switch device on resize events
	css_media_query_enabled 	: true

	# global router
	router 						: new com.adamhayes.Router()

	# main service
	collection					: new com.adamhayes.collection.MainCollection()

	# viewstack
	viewstack  					: null

	# preloader
	preloader 	 				: null

	# top menu view
	topMenu						: null

	# about view
	about 						: null

	# thumbs grid
	projectThumbs 				: null


	constructor:->

		# add a preloader object
		@preloader = new com.adamhayes.view.PreloaderView()

		super()


	#
	# Start Application
	# Add Core Views
	# Fire Resize to set their properties
	# Start History
	# Initialize Thumbs View
	#
	startup: () =>

		# Config a few global variables
		configController = new com.adamhayes.controller.ConfigController @collection.config

		
		# viewstack
		@viewstack = new com.adamhayes.ViewStack()

	
		# Top Menu
		@topMenu = new com.adamhayes.view.TopMenuView {model: @collection.config}


		# Project Thumbs
		@projectThumbs = new com.adamhayes.view.ThumbsView {collection: @collection.thumbsCollection}
		@viewstack.addView "main", @projectThumbs


		# About View
		@about = new com.adamhayes.view.AboutView {model: @collection.aboutModel}
		@viewstack.addView "about", @about


		# Project View
		@project = new com.adamhayes.view.ProjectContainerView {collection: @collection.projectsCollection}
		@viewstack.addView "project", @project

		# is started
		@isStarted = true

		# manually add the resize listener to trigger after the views are rendered
		@viewstack.addResizeListener()

		@dispatcher.trigger "kore.forceresize"

		# Start history
		Backbone.history.start {pushState: true}


	
	onResize:(e)=>
		super e 

		@preloader.reposition @viewport_height
		


	#
	# Override from KoreView
	# Debounced Route Event Listener
	#
	onRoute : (e) ->
		super e

		@viewstack.setActiveView e.action, e.param

		@topMenu.routeChange e.action, e.param


