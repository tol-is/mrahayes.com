###
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
###

class com.adamhayes.controller.ConfigController

	constructor: (configModel) ->

		Config.bgColor = configModel.get("colors").primary_bg_color
		
		Config.copyColor = configModel.get("colors").primary_copy_color

		Config.thumbBgColor = configModel.get("colors").thumb_bg_color
		
		Config.thumbCopyColor = configModel.get("colors").thumb_copy_color