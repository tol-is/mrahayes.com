###
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
###

# Dependancies
#<< com/adamhayes/model/*

class com.adamhayes.collection.MainCollection extends Backbone.Collection
	
	url					: Config.url+'/data'
	
	config 				: null

	aboutModel			: null

	thumbsCollection	: null

	projectsCollection	: null


	parse : (response) ->

		# make a config model
		@config = new com.adamhayes.model.AppConfigModel response.config

		# about view model
		@aboutModel = new com.adamhayes.model.AboutModel response.about_page
		
		# project thumbs collection
		@thumbsCollection = new com.adamhayes.collection.ThumbsCollection response.works
		
		# Project Previews Collection for project view
  		# Only contains slug and bg color
  		# slug for next and previews
  		# bg to show bg container immediately
		projectPreviewsObject = {}
		projectPreviewsObject.projects = _.map response.works, (work) ->
			obj = 
				"slug" 			: work.slug
				"bgColor"  		: work.bgColor
				"primaryColor" 	: work.primaryColor
				"copyColor" 	: work.copyColor
				"cats" 			: work.cats
			return  obj

		previewsModel = new com.adamhayes.model.ProjectPreviewsModel projectPreviewsObject

		# projects collection
		@projectsCollection = new com.adamhayes.collection.ProjectCollection previewsModel

		
		
		@trigger 'collection.loaded'