###
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
###

class com.adamhayes.collection.ProjectCollection extends Backbone.Collection

	currentCategory 		: "all";

	currentProjectSlug 		: ""

	previewsModel 			: {}

	currentPreviews 		: {}

	currentlength 			: 0

	model 					: com.adamhayes.model.ProjectModel


	#
	#
	#
	constructor: (@previewsModel)->
		super()
		
		@filterByCategory()
		
	#
	#
	#
	url: ->
		return Config.url+'/data/works/' + @currentProjectSlug;



	#
	#
	#
	fetchProjectBySlug: (projectslug)->
		@currentProjectSlug = projectslug

		# Although backbone won't add the same model again, it still makes the remote request
		if @projectAlreadyLoaded @currentProjectSlug is true
			@onLoaded()
		else
			@fetch { add: true, success: @onLoaded, error: @onError }

	#
	#
	#
	onLoaded:(e)=>
		@trigger 'collection.loaded', e

	#
	#
	#
	onError:(e)=>	
		@trigger 'collection.error', e


	#
	#
	#
	parse:(response)->
		return response


	#
	#
	#
	getProjectModelBySlug:(slug)->
		obj = _.find @models, (model) ->
			model.get("slug") is slug

	#
	#
	#
	projectAlreadyLoaded:(slug)->
		obj = @getProjectModelBySlug slug

		if obj?
			return true
		else
			return false
	 	

	#
	#
	#
	getCurrentPreviewBySlug:(slug)->
		return _.find @currentPreviews, (proj) ->
			proj.slug is slug

	#
	#
	#
	getNextProject:(currentSlug)->
		index = @getCurrentPreviewIndexBySlug currentSlug

		index = if index < @currentPreviews.length - 1 then index+1 else 0

		return @getProjectSlugByIndex index


	#
	#
	#
	getPrevProject:(currentSlug)->
		index = @getCurrentPreviewIndexBySlug currentSlug

		index = if index > 0 then index-1 else @currentPreviews.length - 1
		
		return @getProjectSlugByIndex index


	#
	#
	#
	getCurrentPreviewIndexBySlug:(slug)->
		currentObj = @getCurrentPreviewBySlug slug
		index = _.indexOf @currentPreviews, currentObj


	#
	#
	#
	getProjectSlugByIndex:(index)->
		return @currentPreviews[index].slug



	#
	#
	#
	getPreviewBySlug:(slug)->
		return _.find @previewsModel.get("projects"), (proj) ->
			proj.slug is slug


	#
	#
	#
	filterByCategory:(category="all")->
		@currentCategory = category
		
		# go through the previews to find which project match the selected category
		# will be used for next previous project
		@currentPreviews = _.filter @previewsModel.get("projects"), (project)->
			return _.indexOf(project.cats, category) >=0

		@currentlength = @currentPreviews.length

