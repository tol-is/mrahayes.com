###
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
###

#<< com/adamhayes/model/project_thumb_model

#
# Thumbs collection is fed with data from the main_collection
#
class com.adamhayes.collection.ThumbsCollection extends Backbone.Collection

	model 			: com.adamhayes.model.ProjectThumbModel

	#
	# Construct
	# Override  constructor to create initial data
	#
	constructor: (data) ->
		super()

		# For each work entry
		_.each data, (entry,order) => 
			
			# push a new project thumb model
			@push new com.adamhayes.model.ProjectThumbModel entry
			
	

	addModels: (array) ->

		_.each array, (item) =>
			@models.push new com.adamhayes.model.ProjectThumbModel(item)

	

		