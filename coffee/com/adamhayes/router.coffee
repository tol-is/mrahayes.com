###
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
###

class com.adamhayes.Router extends Backbone.Router

	#
	# Configure Router
	#
	routes:
		'project/:projectslug'  	: 'project'
		'project/:projectslug/' 	: 'project'
		'category/:catslug'  		: 'category'
		'category/:catslug/' 		: 'category'
		'about' 					: 'about'
		'about/' 					: 'about'
		'*path' 					: 'default'

	#
	# Default Page Request
	#   
	default: () ->
		@dispatcher.trigger "kore.route", {"action":"main"}

	#
	# Project Cateogory Request
	#
	category: (catslug) ->
		@dispatcher.trigger "kore.route", {"action":"main", "param":catslug}

	#
	# Single Project Request
	#
	project: (projectslug) ->
		@dispatcher.trigger "kore.route", {"action":"project", "param":projectslug}

	#
	# About Page Request
	#
	about: () ->
		@dispatcher.trigger "kore.route" , {"action":"about"}

	#
	# Blog Page Request
	#
	blog: () ->
		@dispatcher.trigger "kore.route", {"action":"blog"}