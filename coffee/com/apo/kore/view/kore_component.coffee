###
Author:  ap-o
Author URI: http://www.ap-o.om
###

class com.apo.kore.view.KoreComponent extends Backbone.View

	device			: null

	interactive 	: false

	active 			: false


	#
	# Construct
	#
	constructor: (options) ->
		super options

		@device = Support.device

		

	#
	# Initialize
	#
	initialize: () ->
		super()

		@dispatcher.on "kore.device_change", @onDeviceChange



	#
	# Remove
	# Removes Listeners
	# Undelegates Backbone Events
	# Remove $el from DOM
	#
	remove:->
		@deactivate()
		@undelegateEvents() 
		super()



	# ---------------------------------
	# Activate Deactivate


	#
	# Activate
	# Adds Interactive Listeners
	# and set class active
	#
	activate: ->
		@active = true

		@$el.addClass "active"

		@addKeyListeners()

		if Support.hasTouch
			@addTouchListeners()
		else
			@addMouseListeners()


	#
	# Deactivate
	# Removes Interactive Listeners
	# removes class active
	#
	deactivate: ->
		@active = false
		@$el.removeClass "active"
		
		@removeKeyListeners()

		if Support.hasTouch
			@removeTouchListeners()
		else
			@removeMouseListeners()



	#
	# Add a Listener
	# Used for hammer events e.g. @addListener "tap", @onTap
	# Hold tap swipe doubletap transformstart transform transformend dragstart drag dragend swipe release
	# Mouse are wrapped in addMouseListener
	#
	addListener: (gesture, callback) ->
		@$el.on gesture,callback

	#
	# Remove/Unbind a Listeners
	#
	removeListener: (gesture, callback) ->
		@$el.off gesture, callback


	# ---------------------------------
	# Touch


	enableTouch: (options, forceEnable=false) =>
		if Support.hasTouch or forceEnable
			@$el.hammer options
	

	#
	# Add Listeners
	# Called by activate if its interactive
	# Override in a component
	#
	addTouchListeners: ->
		

	#
	# Remove listeners
	# Called by deactivate if its interactive
	#
	removeTouchListeners: ->
		



	# ---------------------------------
	# MOUSE LISTENERS

	#
	# Add Mouse Listeners
	#
	addMouseListeners: ->
		@cssprop "cursor", "pointer"
		@$el.on "mouseover", @onMouseOver
		@$el.on "mouseout", @onMouseOut
		@$el.on "click", @onMouseClick


	#
	# Remove Mouse Listeners
	#
	removeMouseListeners: ->
		@cssprop "cursor", "auto"
		@$el.off "mouseover", @onMouseOver
		@$el.off "mouseout", @onMouseOut
		@$el.off "click", @onMouseClick


	#
	# On Mouse Over
	#
	onMouseOver: (e) =>
		@$el.addClass "hover"


	#
	# On Mouse Out
	#
	onMouseOut: (e) =>
		@$el.removeClass "hover"

	#
	# On Mouse Click
	#
	onMouseClick: (e) =>



	# ---------------------------------
	# KEYBOARD LISTENERS

	#
	# Add Keyboard Listeners
	#
	addKeyListeners: ->
		


	#
	# Remove Keyboard Listeners
	#
	removeKeyListeners: ->


	

	# ---------------------------------
	# SHOW HIDE


	#
	# Show
	#
	show: (time = 0) ->
		
		TweenLite.to @$el, time,
			css:
				autoAlpha: 1
			ease: Expo.easeOut


	#
	# Hide
	#
	hide: (time = 0) ->
		TweenLite.to @$el, time,
			css:
				autoAlpha: 0
			ease: Expo.easeOut





	# ---------------------------------
	# DEVICE


	#
	# Set Device
	#
	setDevice: (string)->
		@device = string


	#
	# Get Device
	#
	getDevice: ->
		@device


	#
	# onDevice Change
	#
	onDeviceChange : (e) =>
		@device = Support.device


		
	# ---------------------------------
	# CSS Wraps

	#
	# Html
	#
	html: (content) ->
		@$el.html content


	#
	# Append
	#
	append: ($dom) ->
		@$el.append $dom


	#
	#
	#
	prepend: ($dom) ->
		@$el.prepend $dom


	#
	# 
	#
	find:(param)->
		return @$el.find param
	#
	# Set Size
	#
	setSize:(width, height)=>
		@width width
		@height height


	#
	# Set/Get Width
	#
	width: (num) ->
		if num?
			@$el.width num 
		else
			@$el.width()


	#
	# Set/Get Height
	#
	height: (num) ->
		if num?
			@$el.height num 
		else
			@$el.height()


	#
	# Outer Width
	#
	outerHeight: () ->
		@$el.outerWidth()


	#
	# Outer Height
	#
	outerHeight: () ->
		@$el.outerHeight()


	#
	# Inner Width
	#
	innerHeight: () ->
		@$el.innerWidth()


	#
	# Inner Height
	#
	innerHeight: () ->
		@$el.innerHeight()


	#
	# Get Position
	#
	position:->
		@$el.position()


	#
	# Get Offset
	#
	offset:->
		@$el.offset()


	#
	# Add Class
	#
	addClass: (string) ->
		@$el.addClass string


	#
	# Remove Class
	#
	removeClass: (string) ->
		@$el.removeClass string


	#
	# Has Class
	#
	hasClass: (string) ->
		@$el.hasClass string


	#
	# Toggle Class
	#
	toggleClass: (string) ->
		@$el.toggleClass string

		
	#
	# Set/Get Css Property
	#
	cssprop: (prop, value) ->
		if prop? and value?
			@$el.css prop, value
		else if prop?
			@$el.css prop


	#
	# Set/Get Attribute
	#
	attr: (attr, value) ->
		if attr? and value?
			@$el.attr attr, value
		else if attr?
			@$el.attr attr


	#
	# Set/Get Data
	#
	data: (id, value) ->
		if id? and value?
			@$el.data id, value
		else 
			@$el.data id



