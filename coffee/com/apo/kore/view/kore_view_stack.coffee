###
Author:  ap-o
Author URI: http://www.ap-o.om
###

# Dependancies
#<< com/apo/kore/view/kore_component
#<< com/apo/kore/view/kore_view

class com.apo.kore.view.KoreViewStack extends com.apo.kore.view.KoreView

	views 		: {}

	defaultId 	: "main" 

	currentId 	: null

	
	#
	# Add New View
	#
	addView: (id, obj)->
		@views[id] = obj


	#
	# Set Active View
	#
	setActiveView: (id=@defaultId, param=null)->
		
		# hide Current
		if @currentId? and id isnt @currentId
			@views[@currentId].hide()
		
		# show new
		@views[id].show(param)
		
		# hold id
		@currentId = id
