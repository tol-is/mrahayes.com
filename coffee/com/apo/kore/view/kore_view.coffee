###
Author:  ap-o
Author URI: http://www.ap-o.om
###

class com.apo.kore.view.KoreView extends com.apo.kore.view.KoreComponent

	$html						: $ 'html'

	$win						: $ window

	viewport_width		: 0

	viewport_height		: 0 
		

	#
	# Construct
	#
	constructor: (options) ->
		super options


	#
	# Initialize
	# Views that extend need to call their super initialize to addResizeListener 
	# or else manually call addResizeListener
	#  
	initialize: ->
		super()

		@render()
		
		@addResizeListener()

		@collection.on 'collection.loaded', @onData if @collection?
		@collection.on 'collection.error', @onError if @collection?



	#
	# On Data Listener
	#
	onData : () =>
		# dispatch load complete
		@dispatcher.trigger "event.loadcomplete"

	#
	# On onError Listener
	#
	onError : () =>
		# dispatch load complete
		@dispatcher.trigger "event.loadcomplete"

		# dispatch an error
		@dispatcher.trigger "fetch.error"
		


	# -------------------------------------------------#
	# Resize

	#
	# Add Resize Listener
	#
	addResizeListener: () =>
		@dispatcher.on 'kore.resize', @onResize
		

	#
	# Remove Resize Listener
	#
	removeResizeListener : =>
		@dispatcher.off 'kore.resize'


	#
	# On Resize Action
	#
	onResize: (e) =>
		@viewport_width = e.viewport_width
		@viewport_height = e.viewport_height


	# -------------------------------------------------#
	# Scroll

	#
	# Add Scroll Listener
	#
	addScrollListener: (debounce=true) =>
		#debounced scroll action
		debouncescroll = _.debounce(() =>
			@onScroll()
		, 100)

		#bind on scorll
		if debounce is true
			@$win.on 'scroll', _.bind(debouncescroll, @)
		else
			@$win.on 'scroll', @onScroll
		
		
		

	#
	# Remove Scroll Listener
	#
	removeScrollListener : =>
		#unbind on scroll
		@$win.off 'scroll'


	#
	# On Scroll Action
	#
	onScroll: => 