###
Author:  ap-o
Author URI: http://www.ap-o.om
###

# Dependancies
#<< com/apo/kore/*
class com.apo.kore.view.KoreContext extends com.apo.kore.view.KoreComponent


	$html						: $ 'html'

	$win						: $ window

	router 						: null

	collection 					: null

	$viewport_width				: 0

	$viewport_height			: 0

	device						: 'desktop'

	css_media_query_enabled		: false

	isTouch						: false

	hasTransitions				: true

	isDeadBrowser				: false

	isRetina 					: false

	isStarted 					: false

	lastRouteEvent 				: null

	prevRoute 					: null

	newRoute 					: null

	#
	# Construct
	#
	constructor: (options) ->

		# Device Config
		if Device.isTablet is true
			Support.device = 'tablet'
		
		else if Device.isMobile is true
			Support.device = 'mobile'
		
		else
			Support.device = 'desktop'


		# Other Support
		@isTouch = Support.hasTouch

		@hasTransitions = Support.hasTransitions

		@isRetina = Support.isRetina
		
		@isDeadBrowser = Support.isDeadBrowser

		# call super constructor at the end
		super options

	#
	# Construct
	#  
	initialize: ->
		super()

		# Set Support if mediaQueries are Enabled
		Support.mediaQueriesEnabled = @css_media_query_enabled

		# add Listeners
		@addResizeListener()
		@addNavigateListener()
		
		# Trigger onResize once
		@onResize()

		if @router?
			@addRouteListener()

		@fetchConfig()


	#
	# When the show begins
	#
	startup:->
		
		
	

	# -------------------------------------------------#
	# App Config

	fetchConfig:->
		# main config or data collection
		if @collection?
			# add loaded listener
			@collection.on 'collection.loaded', @onData
			# fetch collection
			@collection.fetch()
			# Dispatch event loading
			@dispatcher.trigger "event.loading"
		else
			@startup()


	#
	# On Data Listener
	#
	onData : () =>
		# dispatch load complete
		@dispatcher.trigger "event.loadcomplete"

		# call startup function to instantiate views
		TweenLite.delayedCall 1, @startup


	# -------------------------------------------------#
	# Route

	#
	# Add navigate listener
	# Immediate support is false as it comes from the browser
	#
	addRouteListener: ->
		
		debounceroute = _.debounce((e) ->
			@onRoute(e)
		, 300)
		
		@dispatcher.on 'kore.route', _.bind(debounceroute, @)


	#
	# Debounced on Route Listener
	# must be overriden in core view
	#
	onRoute: (e) ->
	
		@prevRoute = if @newRoute? then @newRoute else e
		@newRoute = e

		if @isStarted is false
			return



	# -------------------------------------------------#
	# Navigate

	#
	# Add navigate listener
	# Immediate call is true as it comes from user interaction
	#
	addNavigateListener: ->
		
		@dispatcher.on 'kore.forceresize', @onResize


		debouncenavigate = _.debounce((e) ->
			@navigate(e)
		, 300, true)

		debouncenavigateback = _.debounce((e) ->
			@navigateBack(e)
		, 300, true)
		
		@dispatcher.on 'kore.navigate', _.bind(debouncenavigate, @)
		@dispatcher.on 'kore.navigate.back', _.bind(debouncenavigateback, @)
		
	#
	# Debounced on Navigate Request
	# Override if there is no router
	#
	navigate: (e) ->
		if @router?
			@router.navigate e, true

	#
	#
	#
	navigateBack:->
		window.history.back()



	# -------------------------------------------------#
	# Resize Content

	#
	# Add Resize Listener
	#
	addResizeListener: () =>
		#debounced resize call
		debounceresice = _.debounce(() =>
			@onResize()
		, 300)

		#bind on resize
		@$win.on 'resize', _.bind(debounceresice, @)
		

	#
	# Remove Resize Listener
	#
	removeResizeListener : =>
		#unbind on resize
		@$win.off 'resize'


	#
	# On Resize Action
	#
	onResize: => 
		#hold variables
		@viewport_width = @$win.width()
		@viewport_height = @$win.height()

		# switch device by width
		if @css_media_query_enabled is true
			@setDevice @mediaQuery @viewport_width

		#dispatch an event with the new viewport dimensions
		@dispatcher.trigger 'kore.resize', {'viewport_width': @viewport_width, 'viewport_height': @viewport_height}


	#
	# Media Query
	# Returns the device based on width value
	#
	mediaQuery: (width) ->
		
		switch true
			when width<768
				return "mobile"
			when width<1030
				return "tablet"
			else
				return "desktop"


	#
	# Set Device
	# Override to trigger a kore event
	#
	setDevice : (string) =>

		# if the new value is different
		if @device isnt string

			#Update Global Variable
			Support.device = string

			#dispatch an event with the new device
			@dispatcher.trigger 'kore.device_change', {'device': string}

