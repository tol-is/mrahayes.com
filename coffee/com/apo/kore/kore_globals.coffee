###
Author:  ap-o
Author URI: http://www.ap-o.om
###

#
# Browser detects.
#
ua = navigator.userAgent.toLowerCase()
Browser =

	chrome      : ua.indexOf('chrome') != -1
	safari      : ua.indexOf('safari') != -1 and ua.indexOf('chrome') == -1
	firefox     : ua.indexOf('firefox') != -1
	ie          : ua.indexOf('msie') != -1
	version     : Number($.browser.version)


#
# Device Detection
#

isTablet = ( /ipad|android 3|sch-i800|playbook|tablet|kindle|gt-p1000|sgh-t849|shw-m180s|a510|a511|a100|dell streak|silk/i.test( navigator.userAgent.toLowerCase() ) )
isMobile = ( /iphone|ipod|android|blackberry|opera mini|opera mobi|skyfire|maemo|windows phone|palm|iemobile|symbian|symbianos|fennec/i.test( navigator.userAgent.toLowerCase() ) )

Device = 
	isTablet 		: isTablet
	isMobile 		: isMobile
	isDesktop 		: if isTablet is false and isMobile is false then true else false


#
# Feature detects.
#
Support =

	# Whether or not the device supports touch events
	hasTouch 				: if Touch? then true else false

	# Whether or not an accelerometer is present
	hasDeviceMotion 		: if typeof window.DeviceMotionEvent != 'undefined' then true else false

	# Whether or not a gyroscope is present
	hasDeviceOrientation 	: if typeof window.DeviceOrientationEvent != 'undefined' then true else false

	# Whether or not the browser supports <canvas>
	hasCanvas				: if window.HTMLCanvasElement? then true else false

	# Whether the browser supports css transitions
	hasTransitions			: Modernizr.csstransitions

	# Whether the device has a retina display
	isRetina				: window.devicePixelRatio > 1

	# Whether browser sucks big time
	# TODO: check for other dead browsers
	isDeadBrowser 			: if Browser.ie and Browser.version <= 8 then true else false

	# Media Query Events
	mediaQueriesEnabled 	: false

	# current device
	device 					: ""

	# scroll object
	scroll 					: if Browser.firefox or Browser.ie then $('html') else $("body");



window.Browser = Browser
window.Device = Device
window.Support = Support
