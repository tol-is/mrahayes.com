###
Author:  ap-o
Author URI: http://www.ap-o.om
###

#
# NUTS & BOLTS
#
# General Kore Utilities
#
#
#
#
# Backbone Global Dispatcher
# Courtesy Michi Kono
#
# http://www.michikono.com/2012/01/11/adding-a-centralized-event-kore-on-backbone-js/
#
# Usage:
# this.kore.bind('event_id', callback)
# this.kore.trigger('event_id')
# 
(->
	return if this.isExtended
	# attaching the Events object to the dispatcher object
	dispatcher = _.extend({}, Backbone.Events, cid: "dispatcher")
	_.each [ Backbone.Collection::, Backbone.Model::, Backbone.View::, Backbone.Router:: ], (proto) ->
		# attaching the kore instance
		_.extend proto, dispatcher: dispatcher
)()



# Mixins for CoffeeScript
# Courtesy jashenkas
#
# https://github.com/jashkenas/coffee-script/wiki/FAQ
#
# Usage:
#
#   class Button
#     onClick: -> # do stuff
#
#   include Button, Options
#   include Button, Events

extend = (obj, mixin) ->
	for name, method of mixin
		obj[name] = method

include = (klass, mixin) ->
	extend klass.prototype, mixin

window.extend = extend
window.include = include


#
# If the console object is not available don't throw errors when we try to do
# console.log()-type things
#
if not window.console?
	window.console = {}
	if not window.console.log? then window.console.log = ->
	if not window.console.warn? then window.console.warn = ->
	if not window.console.error? then window.console.error = ->
	if not window.console.dir? then window.console.dir = ->

#
# Random Number
#
randomNumber = (startRange, endRange) ->
  range = endRange - startRange
  randomNumber = endRange - Math.random() * range
  randomNumber

#
# Random Int
#
randomInt = (startRange, endRange) ->
  range = endRange - startRange
  randomNumber = endRange - Math.ceil(Math.random() * range)
  randomNumber

#
#
#
htmlDecode = (input)->
  e = document.createElement('div');
  e.innerHTML = input;
  
  return e.childNodes[0].nodeValue;


#
# Set _ templates to use Django-like syntax
#
if _?
	_.templateSettings =
		interpolate : /\{\{(.+?)\}\}/g
		evaluate : /\{\%(.+?)\%\}/g


#
# Enable CSS :active styles on touch devices
#
try
	document.addEventListener 'touchstart', (->), true
catch e






#
# Read a page's URL arguments and return them as an object.
#
getUrlArgs = (str) ->

	if not str?
		str = window.location.href

	args = {}
	hash = null

	if str.indexOf('?') == -1
		return null

	hashes = str.slice(str.indexOf('?') + 1).split('&')

	for i in [0...hashes.length]
		hash = hashes[i].split('=')
		args[hash[0]] = hash[1]

	return args

#
# Read a page's URL arguments and return them as an object.
#
stringToFunction = (str) ->
  
  arr = str.split(".")
  
  fn = (window or this)
  
  i = 0
  
  len = arr.length

  while i < len
    fn = fn[arr[i]]
    i++
  throw new Error("function not found")  if typeof fn isnt "function"
  
  fn


#
# Fix Date.now() in IE8
#
if !Date.now
	Date.now = ->
		return new Date().valueOf()

#
# Add Objects to window
#

