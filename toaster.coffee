# => SRC FOLDER
toast 'coffee'

	# EXCLUDED FOLDERS (optional)
	exclude: ['coffee/vendor']

	# => VENDORS (optional)
	vendors: [
		# Core
		'coffee/vendor/jquery/jquery-1.8.3.min.js'
		'coffee/vendor/underscore-1.4.3.min.js'
		'coffee/vendor/backbone-0.9.9.js'
		'coffee/vendor/modernizr-2.5.3.min.js'
		# Tweenlite
		'coffee/vendor/gs/TweenLite.min.js'
		'coffee/vendor/gs/easing/EasePack.min.js'
		'coffee/vendor/gs/plugins/ColorPropsPlugin.min.js'
		'coffee/vendor/gs/plugins/CSSPlugin.min.js'
		'coffee/vendor/gs/plugins/ScrollToPlugin.min.js'
		# Hammer
		'coffee/vendor/hammer-0.6.1.js'
		# Spin
		'coffee/vendor/spin.min.js'
		# Keymaster
		'coffee/vendor/keymaster.js'
		# jQuery plugins
		'coffee/vendor/jquery/jquery.mousewheel.js'
		'coffee/vendor/jquery/jquery.hammer-0.3.js'
		'coffee/vendor/jquery/jquery.freetile-0.2.12.js'
		#'coffee/vendor/jquery/jquery.lorem.js' #user lorem for testing
	]

	# => OPTIONS (optional, default values listed)
	# bare: false
	# packaging: true
	# expose: ''
	minify: true

	# => HTTPFOLDER (optional), RELEASE / DEBUG (required)
	httpfolder: '/'
	release: 'js/adamhayes.js'
	debug: 'js/adamhayes-debug.js'