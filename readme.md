#######################################
#######################################
#######################################
# adam hayes illustrator - mrahayes.com




# App
 
On $(document).ready() a new instance of the core context is instantiated (com.adamhayes.Content). That fires the first request for data and initializes the rest of the actors





# Data

App works on a RESful API provided by wordpress, using its pretty permalinks. Its theme outputs json as plain text





# SEO

Some really dirty php within /seo directory (open at your will)





# Source Code

directory /coffee





# Build Script

*directions are for mac osx.*

Project compiles with coffe-toaster, into js/adamhayes.js. Toaster adds a watch service so as you edit your files the project will be built automatically.

More info here
- https://github.com/serpentem/coffee-toaster

## To compile (via terminal)

	1. $ cd project_folder
	2. $ toaster -w

Alternatively you can use the included shell script

	1. $cd project_folder
	2. source script.sh

## Known build issue

When you navigate to your coffee folders the OS adds those annoying invisible files that mess the builder. In that case the watch service will stop. Or if its just broken, stop it manually (ctlr + c). Then you have to delete all the .DS_STORE files.

	1. $ cd project_folder
	2. find . -name .DS_Store | xargs rm -rf;
	3. toaster -w

Alternatively you can use the included shell script

	1. $cd project_folder
	2. source script.sh
	




# Vendors and build params

Configure the build by editing the file toaster.coffee

Exclude which folders are not coffeescript (typically just vendors). 

If you change the toaster config you have to stop the watch service (ctrl + c) and start it again.



# Less

You only need to compile less/style.less into css/style.css



# Blog

Blog is completely independent, within /blog directory, some trivial jquery and a little less




# Humans

If you work on this project, make sure you update humans.txt. Give some credit to the dev community and of course yourself. 
