<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

define('DB_NAME', 'adamhayes_blog');
//define('DB_NAME', 'adam_app');

/** MySQL database username */
define('DB_USER', 'root');
//define('DB_USER', 'adam_app');

/** MySQL database password */
define('DB_PASSWORD', 'root');
//define('DB_PASSWORD', '@d@mh@y3s2013');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');
//define('DB_HOST', 'localhost');
/*
define('FS_METHOD', 'ftpext');
define('FTP_BASE', '/var/www/vhosts/mrahayes.com/httpdocs/data/');
define('FTP_USER', 'adam');
define('FTP_PASS', '@d@mh@y3s2013');
define('FTP_HOST', 'ftp.enjoythisyeah.com'); 
define('FTP_SSL', false);
*/

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'oE%H+.`kwE;4hx4Zyw/HX76YtDFa~Oi$6g*i^QavONEQ-]}}!-4^~GwD6i?@u!]p');
define('SECURE_AUTH_KEY',  'K}Ud-9&-%J.sGw#amH5#=f1%]>uDX!Seuo[7 Fk#`)iIQd;$+spE)7lJ]SHpV;R2');
define('LOGGED_IN_KEY',    'I10.J&$m<)w^O~Cp= :@5zml8fzPj%u)>IuX#MS8cbc3s*.ZL6S)L/`#^NVDwX[7');
define('NONCE_KEY',        'l*veaA/}k540T1TS~?94}l.jpwdST!<R2#.yc<v.zN=.%Tn=LzSKG^^<Y9YUHlsf');
define('AUTH_SALT',        '{2(=Y^!Ovm@c|5:73HuD>?H5B$R};UV6qS r.].IQBH4f=OODkW,4!0`#pu!Y(?w');
define('SECURE_AUTH_SALT', 'I|rRbCsgEc{vwUxA,]TL;Pcic)8z9#h8<zgk-LM4JYY,g%b]rATI,<+A*RO_`(!C');
define('LOGGED_IN_SALT',   'K5Mrmdt$u:19}%$C%1=oQ(U>&:$g*HL8sanb#e{GJdI0LjIO%jKz4]{*T*b|_,Zy');
define('NONCE_SALT',       '|_Mhf<v%x_|;*&t+UO9)u+RI@y/a[j6<8b[g4jvFd0w3X s#GLmP oGiT-dej0Nu');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ah_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
