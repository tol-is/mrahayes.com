/*
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
*/
$(document).ready(function() {

	var isTablet = /ipad|android 3|sch-i800|playbook|tablet|kindle|gt-p1000|sgh-t849|shw-m180s|a510|a511|a100|dell streak|silk/i.test(navigator.userAgent.toLowerCase());

	var isMobile = /iphone|ipod|android|blackberry|opera mini|opera mobi|skyfire|maemo|windows phone|palm|iemobile|symbian|symbianos|fennec/i.test(navigator.userAgent.toLowerCase());


	$('body').css("opacity",1);

	if(isMobile || isTablet)
	{

		var btn = $('#share-btn');

		var defSvgColor = btn.find("svg").css("fill");
		
		btn.addClass("mobile");

		btn.on('click', function()
		{
			btn.addClass("active")
				.find("svg").css("fill","919191");

			TweenLite.delayedCall(2, function()
			{

				btn.trigger('mouseout')
					.removeClass("active")
					.find("svg").css("fill",defSvgColor);
			})
		})
	}

});