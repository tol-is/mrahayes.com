<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title><?php wp_title(':', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1 , user-scalable=no"/>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />  

	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css.css" type="text/css">

	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">

	<link rel="icon" href="favicon.ico" />
	<link rel="author" href="humans.txt" />
	<link rel="image_src" href="<?php bloginfo('template_directory'); ?>/images/adamhayes.png" />

	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>


<header id="header">
	<a href="http://mrahayes.com"><div id="logo"></div></a>
	<nav id="main-nav">
		<a href="http://preview.ahayes.num42.com/" class="menu-item">Work</a>
		<a href="http://preview.ahayes.num42.com/about" class="menu-item">About</a>
		<a href="http://preview.ahayes.num42.com/blog" class="menu-item blog-item">Blog</a>
	</nav>
</header>



<div id="siteWrapper">


