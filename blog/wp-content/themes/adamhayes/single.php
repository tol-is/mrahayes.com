<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		 <div class="post">

			<div class="column-left">
				<h2><?php the_title(); ?></h2>
				<div class="date"><?php the_date('F j, Y', '',''); ?></div>
				<?php
					$primary_color = get_field("primary_color", "options");
				?>

				<div class="share" id="share-btn">
					<span class="icon-share">
						<svg fill="<?php echo $primary_color; ?>"> <path d="M13.481,9.983c-1.217,0-2.29,0.616-2.924,1.553L6.853,9.479 C6.938,9.175,6.999,8.861,6.999,8.53c0-0.235-0.025-0.464-0.069-0.686l4.077-1.812c0.639,0.636,1.519,1.029,2.491,1.029 c1.95,0,3.531-1.581,3.531-3.531c0-1.95-1.581-3.531-3.531-3.531c-1.95,0-3.531,1.581-3.531,3.531c0,0.254,0.029,0.5,0.08,0.74 L5.989,6.074C5.355,5.431,4.475,5.031,3.5,5.031c-1.933,0-3.5,1.567-3.5,3.5s1.566,3.5,3.5,3.5c0.874,0,1.663-0.332,2.276-0.862 l4.178,2.321c0,0.008-0.002,0.015-0.002,0.023c0,1.949,1.58,3.529,3.529,3.529s3.53-1.58,3.53-3.529S15.43,9.983,13.481,9.983z"/> </svg>
					</span>

					<span class="share-text">Share</span>
						
					<div class="share-links">
						<div class="share-box">
							<a href="http://facebook.com/sharer.php?u=<?php the_permalink() ?>&t=<?php the_title(); ?>" target="_blank">
							<img src="<?php bloginfo('template_directory'); ?>/images/fb_ico.jpg">
							</a>
							
							<a href="http://twitter.com/share?text=<?php the_title();?>&url=<?php the_permalink() ?>" target="_blank">
							<img src="<?php bloginfo('template_directory'); ?>/images/twttr_ico.jpg">
							</a>

							<a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink() ?>&description=<?php the_title();?>" class="pin-it-button" count-layout="none" target="_blank">
							<img src="<?php bloginfo('template_directory'); ?>/images/pntrst_ico.jpg">
							</a>
						</div>
					</div>
				</div>

				

				<div class="post-content">
					<?php the_content(''); ?>
					<div class="clearfix"></div>
				</div>


				 <div class="post-nav">
					<div class="prev"><?php previous_post_link('%link', '<span>&larr;&nbsp;</span>Older'); ?></div>
					<div class="next"><?php next_post_link('%link', 'Newer<span>&nbsp;&rarr;</span>') ?></div>
					<div class="clearfix"></div>
				</div>

			<div class="clearfix"></div>
			</div>
		

			<div class="column-right">
			<?php
				$attachments = new Attachments( 'attachments' );
				if( $attachments->exist() ):
					while( $attachments->get() ) :
						echo "<img src=\"".$attachments->src( 'full' )."\">";
					endwhile;
				endif;
			?>
			</div>

			<div class="clearfix"></div>

		</div>

		
	<?php endwhile; else: ?>

		<p>Sorry, no posts matched your criteria.</p>

<?php endif; ?>



<?php get_footer(); ?>
