<?php get_header(); ?>

	<?php if (have_posts()) : ?>

	<?php while (have_posts()) : the_post(); ?>

		<div class="post">

			<div class="column-left">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				<div class="date"><?php the_date('F j, Y', '',''); ?></div>

				<div class="post-content">
					<?php the_content('Read more<span>&nbsp;&rarr;</span>'); ?>
				</div>
			</div>
		  

			<div class="column-right">
			<?php
				$attachments = new Attachments( 'attachments' );
				if( $attachments->exist() ):
					while( $attachments->get() ) :
						echo "<a href=\"".get_permalink()."\"> <img src=\"".$attachments->src( 'full' )."\"></a>";
					endwhile;
				endif;
			?>
			</div>

			<div class="clearfix"></div>

		</div>

		<?php endwhile; ?>

	<div id="navigator">
	  <div class="prev"><?php next_posts_link('&laquo; Older') ?></div>
	  <div class="next"><?php previous_posts_link('Newer &raquo;') ?></div>
	</div>

	<?php else : ?>

		<h2>Not Found</h2>
		<p>Sorry, but you are looking for something that isn't here.</p>

	<?php endif; ?>

  </div>


<?php get_footer(); ?>
