<?php include("seo/seo.php"); ?>

<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title><?php echo $title;?></title>
	<meta name="description" content="<?php echo $metadescription;?>" />
	<meta name="keywords" content="<?php echo $metakeywords;?>" />

	<meta property="og:title" content="<?php echo $title;?>"/>
	<meta property="og:url" content="<?php echo $url; ?>"/>
	<meta property="og:site_name" content="Adam Hayes"/>
	<meta property="og:type" content="illustrator" />
	<meta property="og:email" content="mrahayes@me.com"/>
	<meta property="og:phone_number" content="+44 (0) 778 657 8245"/>
	<meta property="og:twitter" content="mrahayes"/>
<?php foreach ($metaImages as $metaimg): ?>
	<meta property="og:image" content="<?php echo $metaimg; ?>"/>
<?php endforeach; ?>
	<meta property="og:description" content="<?php echo $ogdescription; ?>"/>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1 , user-scalable=no"/>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="/apple-touch-icon-precomposed.png">

	<link rel="icon" href="/favicon.ico" />
	<link rel="author" href="/humans.txt" />
	<link rel="image_src" href="<?php echo $imageSrc;?>" />

	<link rel="stylesheet" href="/css/style.css" type="text/css">

</head>
<body>

	<div id="preloader"></div>

	<div id="main">
		
		<div id="top-menu-view">	
			<div id="categories-menu"></div>
			<div id="logo"></div>
			<div id="main-menu"></div>
		</div>

		<div id="about-view">
			<div id="about"></div>
			<div id="commissions"></div>
			<div id="contact"></div>
			<div class="clearfix"></div>
		</div>

		<div id="project-view"></div>
		
		<div id="thumbs-grid-view">
			<div id="thumbs-container"></div>
		</div>

	</div>	
	
	<div id="seo"><?php echo $seoMarkup; ?></div>
	
	<script type="text/javascript">
		WebFontConfig = { fontdeck: { id: '26962' } }; //live: 28141 // local: 26962

		(function() {
			var wf = document.createElement('script');
			wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
			'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		})();
	</script>

	<script type="text/javascript">
		var _gaq = _gaq || [];
		//_gaq.push(['_setAccount', 'UA-738348-1']);
		//_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>
	
	<script src="/js/config.js"></script>
	<script src="/js/adamhayes.js"></script>

</body>
</html>
