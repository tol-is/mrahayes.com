/*
Site Name:  Adam Hayes
Site URI: http://mrahayes.com
Author:  ap-o
Author URI: http://www.ap-o.om
*/


var Config = {
  
  url                   : "http://adamhayes.local",
  
  bgColor               : "#00ffff",
  
  copyColor             : "#ffffff",
  
  thumbBgColor          : "#ffff00",
  
  thumbCopyColor        : "#ffffff",
  
  bulletColor           : "#ffffff",

  slideshowTimer        : 4
};

window.Config = Config;
