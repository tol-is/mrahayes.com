<?php

    include('classes/db.php');
    include('classes/api.php');
    include('classes/project.php');
    include('classes/about.php');


    $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $output = rtrim($url, '/');
    $path = explode('/', $output);


    $section = $path[count($path) - 1];
    $projectid = "";

    if(count($path) === 5 && $path[3] === "project")
    {
        $section = $path[count($path) - 2];
        $projectid = $path[count($path) - 1];
    }

    if(count($path) === 5 && $path[3] === "category")
    {
        $section = "home";
        $category = $path[count($path) - 1];
    }

    $baseurl = "http://".$path[2];

    $isRoot = 1;

    $pos = strrpos($section, $path[2]);

    if ($pos === false)
    {
       $isRoot = 0;

    }else{

        $isRoot = 1;
    }
    


    $title = 'Adam Hayes, Illustrator';
    $metadescription = 'Adam Hayes is an illustrator, designer and graduate of the Royal College of Art. He currently works from his quiet studio in England\'s Peak District.';
    $metakeywords = "Adam Hayes, Illustrator, London, Uk, Typography, Maps";
    $imageSrc = $baseurl."/images/screen.png";
    $ogdescription = 'Adam Hayes is an illustrator, designer and graduate of the Royal College of Art. He currently works from his quiet studio in England\'s Peak District.';
    $seoMarkup = '';
    $metaImages = array();
    $post = null;
    
    $api = new API();



    if($section == 'project')
    {

            $post = $api->getProject($projectid);
    
            $title = $post->title." by Adam Hayes";
            $metadescription = $post->metadescription;
            $metakeywords = $post->metakeywords;

            $imageSrc = $baseurl."/data/wp-content/uploads/".$post->gallery[0];

            $seoMarkup = "<article><h1>".$post->title." by Adam Hayes</h1>";
            $seoMarkup.= "<p>".$post->description."</p>";
            
            foreach ($post->gallery as $img)
            {
                array_push($metaImages, $baseurl."/data/wp-content/uploads/".$img."\"");
                $seoMarkup.= "<img src=\"".$baseurl."/data/wp-content/uploads/".$img."\" >";
            }

            $seoMarkup .="</article>";
            $ogdescription = preg_replace("/[\n\r]/","",$post->description);

    }
    else if($section == 'home' || $isRoot == 1)
    {


        $posts = $api->getProjects($category);

        $meta = $api->getDefaultMeta();

        $title = "Adam Hayes, Illustrator - Designer";
        $metadescription = $meta["metadescription"];
        $metakeywords = $meta["metakeywords"];
        $imageSrc = $baseurl."/images/screen.png";
        $metaImages = array();

        $totalPosts = count($posts);
        
        for ($i=0; $i < $totalPosts; $i++)
        {

            $seoMarkup.= "<article><a href=\"".$posts[$i]->link."\">";
            
            if($i<10)
            {
                array_push($metaImages, $baseurl."/data/wp-content/uploads/".$posts[$i]->thumbnail."\"");

                $seoMarkup.= "<img src=\"".$baseurl."/data/wp-content/uploads/".$posts[$i]->thumbnail."\" >";
            }
            $seoMarkup.= "<p>".$posts[$i]->title."</p>";
            $seoMarkup.="</a></article>";

        }


        $about = $api->getAbout('about');
        $ogdescription = preg_replace("/[\n\r]/","",$about->main_copy);


    }
    else
    {

        $post = $api->getAbout($section);

        $title = "About Adam Hayes";
        $metadescription = $post->metadescription;
        $metakeywords = $post->metakeywords;
        $imageSrc = $baseurl."/images/screen.png";
        $metaImages = array();

        $ogdescription = $post->main_copy;

        $seoMarkup = "<section>";
        $seoMarkup .= "<article><h1>About</h1><p>".$post->main_copy."</p></article>";
        $seoMarkup .= "<article><h2>Selected Clients</h1><p>".$post->selected_clients_copy."</p></article>";
        $seoMarkup .= "<article><h2>Commissions</h1><p>".$post->commissions_copy."</p></article>";
        $seoMarkup .= "<article><h2>Contact</h1><p>".$post->contact_copy."<br><br>".$post->twitter."<br><br>".$post->copyrights."</p></article>";
        $seoMarkup .= "</section>";
     
    }

    $ogdescription = htmlspecialchars($ogdescription);

    $seoMarkup = preg_replace("/[\n\r]/","",$seoMarkup);

    
?>
