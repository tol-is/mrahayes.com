<?php
/**
 * Responsible for retrieving all the necessary data from the wp database
 *
 * @class API
 * @author Apostolos Christodoulou
 * @version 1.0
 */
class API {

    /**
     * Constants
     */


    protected static $DB_USERNAME = 'root';
    protected static $DB_PASSWORD = 'root';
    protected static $DB_HOST = '127.0.0.1';
    protected static $DB_NAME = 'adamhayes_db';

    /*
    protected static $DB_USERNAME = 'adam_app';
    protected static $DB_PASSWORD = '@d@mh@y3s2013';
    protected static $DB_HOST = 'localhost';
    protected static $DB_NAME = 'adam_app';
    */
     
    public $posts = array();
    public $meta = array();

    public function getProject($projectId = null) {

        $db = new DB();
        $db->connect(self::$DB_USERNAME, self::$DB_PASSWORD, self::$DB_HOST, self::$DB_NAME);

        $query = "SELECT * FROM ah_posts
        WHERE post_status='publish'
        AND post_type='ahayes_project'
        AND post_name='".$projectId."'";

        $result = mysql_query($query) or die('Class '.__CLASS__.' -> '.__FUNCTION__.' : ' . mysql_error());
        $row = mysql_fetch_row($result, MYSQL_ASSOC);

        $post = $this->createFullProject($row);
        
        unset($result);

        $db->disconnect();
        unset($db);

        return $post;

    }

    public function getProjects($category) {

        // TODO Look for categories

        $posts = array();

        $db = new DB();
        $db->connect(self::$DB_USERNAME, self::$DB_PASSWORD, self::$DB_HOST, self::$DB_NAME);

        $query = "SELECT * FROM ah_posts
        WHERE post_status='publish'
        AND post_type='ahayes_project'";

        $result = mysql_query($query) or die('Class '.__CLASS__.' -> '.__FUNCTION__.' : ' . mysql_error());
        while($row = mysql_fetch_array($result, MYSQL_ASSOC))
        {


            $post = $this->createListProject($row);
            $posts[] = $post;
                       
        }
        
        unset($result);

        $db->disconnect();
        unset($db);

        return $posts;

    }


    protected function createFullProject($row) {

        $post = new Project($row);

        // Get the attachments
        $queryAt = "SELECT * FROM ah_postmeta
        WHERE post_id=".$row['ID'];

        $resultAt = mysql_query($queryAt) or die('Class '.__CLASS__.' -> '.__FUNCTION__.' : ' . mysql_error());
        while($rowAt = mysql_fetch_array($resultAt, MYSQL_ASSOC)){

            if($rowAt['meta_key'] === 'thumb_settings_0_thumbnail') {


                $queryThumbnail = "SELECT * FROM ah_postmeta
                WHERE post_id=" . $rowAt['meta_value'];

                $resultTh = mysql_query($queryThumbnail) or die('Class '.__CLASS__.' -> '.__FUNCTION__.' : ' . mysql_error());

                while($rowTh = mysql_fetch_array($resultTh, MYSQL_ASSOC)){

                    if($rowTh['meta_key'] === '_wp_attached_file') {
                        $post->thumbnail = $rowTh['meta_value'];
                    }
                }

            }

            else if($rowAt['meta_key']==="project_copy"){

                $post->description =  preg_replace("/[\n\r]/","",$rowAt['meta_value']);
            }


            else if ( strrpos($rowAt['meta_key'],"project_content") >=0 &&  strrpos($rowAt['meta_key'],"project_image") >0 && substr($rowAt['meta_key'], 0,1) !="_"){


                $queryimage = "SELECT meta_value FROM ah_postmeta
                WHERE meta_key='_wp_attachment_metadata'
                AND post_id='".$rowAt['meta_value']."'";


                $resultImg = mysql_query($queryimage) or die('Class '.__CLASS__.' -> '.__FUNCTION__.' : ' . mysql_error());
                while($rowImg = mysql_fetch_array($resultImg, MYSQL_ASSOC)){

                    $imageobject = unserialize($rowImg['meta_value']);

                    $file = $imageobject["file"];
                    $path = rtrim($file, '/');
                    $path = explode('/', $path);
                    $post->gallery[] = $path[0]."/".$path[1]."/".$imageobject["sizes"]["thumbnail"][file];
                }


            }

            else if($rowAt['meta_key']==="seo_description"){
                $post->metadescription = $rowAt['meta_value'];
            }

            else if($rowAt['meta_key']==="seo_keywords"){
                $post->metakeywords = $rowAt['meta_value'];
            }

        }

        unset($resultAt);
        return $post;
    }

    protected function createListProject($row) {

        $post = new Project($row);

        // Get the post meta
        $queryAt = "SELECT * FROM ah_postmeta
        WHERE post_id=".$row['ID'];

        $resultAt = mysql_query($queryAt) or die('Class '.__CLASS__.' -> '.__FUNCTION__.' : ' . mysql_error());
        while($rowAt = mysql_fetch_array($resultAt, MYSQL_ASSOC)){

            if($rowAt['meta_key'] === 'thumb_settings_0_thumbnail') {

                $queryThumbnail = "SELECT * FROM ah_postmeta
                WHERE post_id=" . $rowAt['meta_value'];

                $resultTh = mysql_query($queryThumbnail) or die('Class '.__CLASS__.' -> '.__FUNCTION__.' : ' . mysql_error());

                while($rowTh = mysql_fetch_array($resultTh, MYSQL_ASSOC)){

                    if($rowTh['meta_key'] === '_wp_attached_file') {
                        $post->thumbnail = $rowTh['meta_value'];
                    }
                }

            }
           
        }

        unset($resultAt);
        
        return $post;
    }


    public function getAbout($slug){

        $db = new DB();
        $db->connect(self::$DB_USERNAME, self::$DB_PASSWORD, self::$DB_HOST, self::$DB_NAME);

        $query = "SELECT * FROM ah_posts
        WHERE post_status='publish'
        AND post_type='page'
        AND post_name='".$slug."'";

        $result = mysql_query($query) or die('Class '.__CLASS__.' -> '.__FUNCTION__.' : ' . mysql_error());
        $row = mysql_fetch_row($result, MYSQL_ASSOC);

        $post = new About($row);
        
        // Get the post meta
        $queryAt = "SELECT * FROM ah_postmeta WHERE post_id=".$row['ID'];

        $resultAt = mysql_query($queryAt) or die('Class '.__CLASS__.' -> '.__FUNCTION__.' : ' . mysql_error());
        while($rowAt = mysql_fetch_array($resultAt, MYSQL_ASSOC)){
            
            if($rowAt['meta_key'] === 'top_page_main_copy')
            {
                 $post->main_copy = $rowAt['meta_value'];
            }
            else if($rowAt['meta_key'] === 'top_page_selected_clients_copy') 
            {
                $post->selected_clients_copy = $rowAt['meta_value'];
            }
            else if($rowAt['meta_key'] === 'top_page_commisions_copy') 
            {
                $post->commissions_copy = $rowAt['meta_value'];
            }
            else if($rowAt['meta_key'] === 'top_page_contact_copy') 
            {
                $post->contact_copy = $rowAt['meta_value'];
            }
            else if($rowAt['meta_key'] === 'twitter_link') 
            {
                $post->twitter = $rowAt['meta_value'];
            }
            else if($rowAt['meta_key'] === 'copyrights') 
            {
                $post->copyrights = $rowAt['meta_value'];
            }

            else if($rowAt['meta_key']==="seo_description"){
                $post->metadescription = $rowAt['meta_value'];
            }

            else if($rowAt['meta_key']==="seo_keywords"){
                $post->metakeywords = $rowAt['meta_value'];
            }

        }

        unset($result);

        $db->disconnect();
        unset($db);

        return $post;
    }

    public function getDefaultMeta()
    {
        $db = new DB();
        $db->connect(self::$DB_USERNAME, self::$DB_PASSWORD, self::$DB_HOST, self::$DB_NAME);

        $descQuery = "SELECT option_value FROM ah_options 
        WHERE option_name='options_seo_description'";

        $descRes = mysql_query($descQuery) or die('Class '.__CLASS__.' -> '.__FUNCTION__.' : ' . mysql_error());
        $descRow = mysql_fetch_row($descRes, MYSQL_ASSOC);

        $meta["metadescription"] = $descRow["option_value"];


        $keysQuery = "SELECT option_value FROM ah_options 
        WHERE option_name='options_seo_keywords'";

        $keysRes = mysql_query($keysQuery) or die('Class '.__CLASS__.' -> '.__FUNCTION__.' : ' . mysql_error());
        $keysRow = mysql_fetch_row($keysRes, MYSQL_ASSOC);

        $meta["metakeywords"] = $keysRow["option_value"];

        return $meta;
        
    }

    

}
?>
