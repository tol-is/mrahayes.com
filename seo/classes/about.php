<?php
/**
 * The post class is responsible for parsing the data of a page
 *
 * @class Page
 * @author Apostolos Christodoulou
 * @version 1.0
 */
class About {

    /**
     * Constants
     */
    public static $SORT_DATE_ASC = 'post_date ASC';
    public static $SORT_DATE_DESC = 'post_date DESC';

    /**
     * Public variables
     */
    public $id = 0;
    public $title = '';
    public $link = '';
    
    public $main_copy = '';
    public $selected_clients_copy = '';
    public $commissions_copy = '';
    public $contact_copy = '';
    public $twitter = '';
    public $copyrights = '';

    public $metadescription = '';
    public $metakeywords = '';
    
    

    /**
     * Constructor
     * @param {Array} $object The object that we need to parse
     * @author Apostolos Christodoulou
     */
    public function __construct($object) {
        global $baseurl;

        $this->id = $object['ID'];
        $this->title = $object['post_title'];
        $this->link = $baseurl."/project/".$object['post_name'];

    }

    public function addMeta($key, $value){
        $this->meta[$key] = $value;
    }

}
?>
