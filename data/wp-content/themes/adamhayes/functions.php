<?php

/*
1. library/bones.php
    - head cleanup (remove rsd, uri links, junk css, ect)
*/
require_once('library/config.php');


/*
2. library/custom-post-type.php
    - an example custom post type
*/
require_once('library/custom-post-types.php'); // you can disable this if you like


/*
3. library/admin.php
    - removing some default WordPress dashboard widgets
    - changing text in footer of admin
*/
require_once('library/admin.php'); // this comes turned off by default



/************* THUMBNAIL SIZE OPTIONS *************/

	add_image_size( "thumb_100", 100, 9999, false );
	add_image_size( "thumb_200", 200, 9999, false );
	add_image_size( "thumb_300", 300, 9999, false );
	add_image_size( "thumb_400", 400, 9999, false );
	add_image_size( "thumb_600", 600, 9999, false );



	add_image_size( "img_1200", 1200, 1200, false );
	add_image_size( "img_900", 900, 900, false );
	add_image_size( "img_700", 700, 700, false );
	add_image_size( "img_400", 400, 400, false );


?>