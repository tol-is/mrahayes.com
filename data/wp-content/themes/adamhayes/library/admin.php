<?php

/************* DASHBOARD WIDGETS *****************/

// removing the dashboard widgets
add_action('admin_menu', 'disable_default_dashboard_widgets');

// disable default dashboard widgets
function disable_default_dashboard_widgets() {
	remove_meta_box('dashboard_right_now', 'dashboard', 'core');    // Right Now Widget
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'core'); // Comments Widget
	remove_meta_box('dashboard_incoming_links', 'dashboard', 'core');  // Incoming Links Widget
	remove_meta_box('dashboard_plugins', 'dashboard', 'core');  

	remove_meta_box('dashboard_quick_press', 'dashboard', 'core');  // Quick Press Widget
	remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core');   // Recent Drafts Widget
	remove_meta_box('dashboard_primary', 'dashboard', 'core');         //
	remove_meta_box('dashboard_secondary', 'dashboard', 'core');       //
}


add_action('admin_menu','admin_menu_separator');
function admin_menu_separator() {
	//add_admin_menu_separator(3);
	//add_admin_menu_separator(5);
	//add_admin_menu_separator(9);
	
	
}

function add_admin_menu_separator($position) {
  global $menu;
  $index = 0;
  foreach($menu as $offset => $section) {
    if (substr($section[2],0,9)=='separator')
      $index++;
    if ($offset>=$position) {
      $menu[$position] = array('','read',"separator{$index}",'','wp-menu-separator');
      break;
    }
  }
  ksort( $menu );
}




/************* CUSTOMIZE MENU *******************/

//Customize the admin menu
add_action( 'admin_menu', 'punchdrunk_menu' );

//Add our own items
function punchdrunk_menu() {

	//remove items from default menu
	remove_menus();
	add_menu_page('About', 'About', 'moderate_comments', 'post.php?post=22&action=edit', '',   get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png', 10);
}

//Remove a few default pages
function remove_menus()
{
    global $menu;
    global $current_user;
    get_currentuserinfo();
	
    if($current_user->ID != '1')
    {
        $restricted = array(__('Posts'),
                            //__('Media'),
                            __('Links'),
                            //__('Pages'),
                            __('Comments'),
                            __('Appearance'),
                            __('Plugins'),
                            //__('Users'),
                            __('Tools'),
                            __('Settings')
        );
        end ($menu);
        while (prev($menu)){
            $value = explode(' ',$menu[key($menu)][0]);
            if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
        }

   }
}

//Add custom css
add_action('admin_enqueue_scripts', 'punchdrunk_admin_css', 11 );

function punchdrunk_admin_css() {
	echo  "<link type='text/css' rel='stylesheet' href='" . get_template_directory_uri() . '/library/css/admin.css' . "' />";

	if(isset($_GET['post']))
	{
		$staticids = array(30,51,52);
		$postid = $_GET['post'];

		if (in_array($postid, $staticids)) {
    		echo  "<link type='text/css' rel='stylesheet' href='" . get_template_directory_uri() . '/library/css/static-pages.css' . "' />";
		}
	}


	//

}


/************* CUSTOMIZE ADMIN *******************/

// adding it to the admin area
add_filter('admin_footer_text', 'punchdrunk_custom_admin_footer');

// Custom Backend Footer
function punchdrunk_custom_admin_footer() {
	echo '<span id="footer-thankyou">Developed by <a href="http://ap-o.com" target="_blank">ap-o.com</a></span>';
}

?>