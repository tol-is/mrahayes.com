<?php
/* Bones Custom Post Type Example
This page walks you through creating 
a custom post type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

Developed by: Eddie Machado
URL: http://themble.com/bones/
*/


// let's create the function for the custom type
function custom_post_types() { 


	register_post_type( 'ahayes_project', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Works', 'mrahayes'), /* This is the Title of the Group */
			'singular_name' => __('Project', 'mrahayes'), /* This is the individual type */
			'all_items' => __('Works', 'mrahayes'), /* the all items menu item */
			'add_new' => __('Add New Project', 'mrahayes'), /* The add new menu item */
			'add_new_item' => __('Add New Project', 'mrahayes'), /* Add New Display Title */
			'edit' => __( 'Edit Project', 'mrahayes' ), /* Edit Dialog */
			'edit_item' => __('Edit Project', 'mrahayes'), /* Edit Display Title */
			'new_item' => __('New Project', 'mrahayes'), /* New Display Title */
			'view_item' => __('View Works', 'mrahayes'), /* View Display Title */
			'search_items' => __('Search Works', 'mrahayes'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'mrahayes'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'mrahayes'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the works post type', 'mrahayes' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'works', 'with_front' => false ), /* you can specify its url slug */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title')
			//'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */

	    register_taxonomy( 'project_cat', 
    	array('ahayes_project'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => true,     /* if this is true, it acts like categories */             
    		'labels' => array(
    			'name' => __( 'Project Categories', 'bonestheme' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Project Category', 'bonestheme' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Project Categories', 'bonestheme' ), /* search title for taxomony */
    			'all_items' => __( 'All Project Categories', 'bonestheme' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Project Category', 'bonestheme' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Project Category:', 'bonestheme' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Project Category', 'bonestheme' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Project Category', 'bonestheme' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Project Category', 'bonestheme' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Project Category Name', 'bonestheme' ) /* name title for taxonomy */
    		),
    		'show_ui' => true,
    		'query_var' => false,
    		'rewrite' => array( 'slug' => 'project_cat' ),
    	)
    );  
} 

	// adding the function to the Wordpress init
	add_action( 'init', 'custom_post_types');
	

?>