<?php
/*
Template Name: About Page
Authod: ap-o
*/
get_header();                                                                            

	$response = array();

	// Config Color Settings
	$config = array();

	$wp_color_settings = get_field("color_settings", 'options');
	$config["colors"] = $wp_color_settings[0];
	
	// Add Main to Response
	$main_menu = array();
	include("acf/main-menu.php");
	$config["main_menu"] = $main_menu;

	// Add Config to Response
	$response["config"] = $config;

	$about_page = array();
	include("acf/about-page.php");
	$response["about_page"] = $about_page;
	echo json_encode($response);


?>