<?php
/*
Template Name: Home Page
*/
get_header();
	
	$response = array();

	// Config Color Settings
	$config = array();

	$wp_color_settings = get_field("color_settings", 'options');
	$config["colors"] = $wp_color_settings[0];
	
	
	$allcats = array();
	include("acf/cats.php");
	$config["project_cats"] = $allcats;

	// Add Main to Response
	$main_menu = array();
	include("acf/main-menu.php");
	$config["main_menu"] = $main_menu;

	// Add Config to Response
	$response["config"] = $config;

	// About Page
	$about_page = array();
	include("acf/about-page.php");
	$response["about_page"] = $about_page;


	// Ger Projects
	$do_not_show_stickies = 1; // 0 to show stickies
	$args=array(
		'post_type' => array('ahayes_project'),
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'posts_per_page' => -1,
		'caller_get_posts' => $do_not_show_stickies
	);
	$wp_query = new WP_Query($args);
	$maxnumpages = 3;


	// If we have projects
	if (have_posts()) : 

	$works = array();

		while (have_posts()) : the_post();

			include("acf/project-preview.php");
			$works[] = $row_post;

    	endwhile;
		
		$response["works"] = $works;

    endif;

    // ech as json
    echo json_encode($response);
?>