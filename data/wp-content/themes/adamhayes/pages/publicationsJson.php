<?php

	$row = array();

	$row['id'] = $post->ID;
	$row['permalink'] = $post->post_name;
	$row['caption'] = get_field("publication_caption");

	$coverID = get_field("publication_cover");
	$pageID = get_field("publication_page");
	$row['cover'] = wp_get_attachment_image_src( $coverID,"large");
	$row['page'] = wp_get_attachment_image_src( $pageID,"large");

	$row['title'] = get_field("publication_title");
	$row['date'] = get_field("publication_issue");

	$data['publications'][] = $row;	
?>
