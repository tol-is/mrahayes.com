<?php

	 $catargs = array(
	'type'                     => 'project',
	'orderby'                  => 'name',
	'order'                    => 'ASC',
	'taxonomy'                 => 'project_cat',
	'pad_counts'               => false );

	$categories = get_categories( $catargs );

	foreach ($categories as $category) {
		
		$row = array();
		
		$row['id'] = $category->term_id;
		$row['name'] = $category->name;
		$row['slug'] = $category->slug;
	
		$data['categories']["rows"][] = $row;
}


	//term_id
	//name
	//slug 
	//print_r($categories);

?>