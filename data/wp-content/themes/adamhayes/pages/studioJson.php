<?php

	
	$row = array();

		
	while(the_flexible_field("grid_content")):

		$rowContent = array();
		
		$rowType = get_row_layout();
		
		$rowContent['type'] = $rowType;
		
		switch ($rowType) {
			
			case 'grid_image_box':
				$rowContent['image_object'] = get_sub_field("grid_image");

				$sizes = $rowContent['image_object']['sizes'];
				
				foreach($sizes as $size){
					
					$key = key($sizes);
	
					$img = wp_get_attachment_image_src( $rowContent['image_object']['id'],$key);
					
				   	$rowContent['image_object']['sizes'][$key] = $img;
				    next($sizes); 
				}

				
				$rowContent['image_width'] = get_sub_field("grid_image_width");
				break;
			
			case 'grid_push_box':
				$rowContent['push_width'] = get_sub_field("grid_push_width");
				break;
			case 'grid_red_box':
				$rowContent['red_box_width'] = get_sub_field("red_box_width");
				$rowContent['red_box_ratio'] = get_sub_field("red_box_ratio");
				$rowContent['red_box_custom_height'] = get_sub_field("red_box_custom_height");
				break;
			case 'grid_copy_box':
				$rowContent['copy'] = get_sub_field("grid_copy");
				$rowContent['copy_width'] = get_sub_field("grid_copy_width");
				$rowContent['copy_pos'] = get_sub_field("grid_copy_pos");

				break;

			case 'grid_quote_box':
				$rowContent['quote_copy'] = get_sub_field("grid_quote_copy");
				$rowContent['quote_width'] = get_sub_field("grid_quote_width");
				$rowContent['quote_size'] = get_sub_field("grid_quote_text_size");
				break;
			case 'grid_line_break':
				$rowContent['break_height'] = get_sub_field("grid_line_break_height");
				break;
		}


		$row[] = $rowContent;

	endwhile;

	$data['grid'] = $row;

?>