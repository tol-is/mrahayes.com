<?php

	$row_post = array();

	$row_post['id'] = $post->ID;
	$row_post['slug'] = $post->post_name;
	$row_post['title'] = $post->post_title;
	$row_post['bgColor'] = get_field('bg_color');
	$row_post['primaryColor'] = get_field('primary_color');
	$row_post['copyColor'] = get_field('copy_color');

	// post cats
	$cats = array();
	include("project-cats.php"); 
	$row_post['cats'] = $cats;

	// project thumb
	$thumb = array();
	include("project-thumb.php"); 
	$row_post['thumb'] = $thumb;

	// flexible content
