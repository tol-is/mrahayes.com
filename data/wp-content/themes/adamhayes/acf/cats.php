<?php

	$categories = get_terms( 'project_cat' );

	$allcats = array();

	if($categories):
		
		foreach( $categories as $cat ):
		
			$catobj = array();
			$catobj["id"] = $cat->term_id;
			$catobj["name"] = $cat->name;
			$catobj["slug"] = $cat->slug;
			
			$allcats[] = $catobj;
		
		endforeach;
	
	endif;
