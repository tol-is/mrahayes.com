<?php

	// Main Menu
	$main_menu = array();


	// Home/Main/Works Link
	$main_post = get_field("main_page", "options");

	$main_item = array();
	$blog_item["type"] = "internal";
	$main_item["id"] = $main_post->ID;
	$main_item["slug"] ="/";
	$main_item["title"] = $main_post->post_title;
	
	$main_menu["main"] = $main_item;


	// About Link
	$about_post = get_field("about_page", "options");

	$about_item = array();
	$blog_item["type"] = "internal";
	$about_item["id"] = $about_post->ID;
	$about_item["slug"] = $about_post->post_name;
	$about_item["title"] = $about_post->post_title;

	$main_menu["about"] = $about_item;


	// Blog Link
	$blog_item = array();
	$blog_item["type"] = "external";
	$blog_item["slug"] = get_field("blog_page", "options");
	$blog_item["title"] = "Blog";
	
	$main_menu["blog"] = $blog_item;