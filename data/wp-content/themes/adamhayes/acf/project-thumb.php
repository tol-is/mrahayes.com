<?php
		
	$thumb_config_row = get_field('thumb_settings');
	$thumb_config_row = $thumb_config_row[0];

	$thumb = array();


	$imgId = $thumb_config_row["thumbnail"];

	$wpthumb = wp_get_attachment_image_src($imgId, "full");
	$thumb_600 = wp_get_attachment_image_src($imgId, "thumb_600");
	$thumb_400 = wp_get_attachment_image_src($imgId, "thumb_400");
	$thumb_300 = wp_get_attachment_image_src($imgId, "thumb_300");
	$thumb_200 = wp_get_attachment_image_src($imgId, "thumb_200");

	#$thumb["src"] = str_replace("http://adamhayes.local/", "http://preview.ahayes.num42.com/", $wpthumb[0] ) ;
	$thumb["t_600"] = str_replace("http://adamhayes.local/", "/", $thumb_600[0] ) ;
	$thumb["t_400"] = str_replace("http://adamhayes.local/", "/", $thumb_400[0] ) ;
	$thumb["t_300"] = str_replace("http://adamhayes.local/", "/", $thumb_300[0] ) ;
	$thumb["t_200"] = str_replace("http://adamhayes.local/", "/", $thumb_200[0] ) ;


	$thumb["width"] = $wpthumb[1];
	$thumb["height"] = $wpthumb[2];
	$thumb["thumb_size"] = $thumb_config_row["thumb_size"];
