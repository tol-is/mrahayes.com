<?php

	$about_post = get_field("about_page", "options");
	$about_post_id = $about_post->ID;

	$about_page = array();
	$about_page["main_copy"] = get_field("top_page_main_copy",$about_post_id);
	$about_page["selected_clients_copy"] = get_field("top_page_selected_clients_copy",$about_post_id);
	$about_page["commisions_copy"] = get_field("top_page_commisions_copy",$about_post_id);
	$about_page["contact_copy"] = get_field("top_page_contact_copy",$about_post_id);
	$about_page["twitter_link"] = get_field("twitter_link",$about_post_id);
	$about_page["copyrights"] = get_field("copyrights",$about_post_id);