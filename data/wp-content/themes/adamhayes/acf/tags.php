<?php

	$wptags = wp_get_post_tags($post->ID);

	if($wptags):

		foreach( $wptags as $tag ):
		
			$tagobj = array();
			$tagobj["id"] = $tag->term_id;
			$tagobj["name"] = $tag->name;
			$tagobj["slug"] = $tag->slug;
			$tags[] = $tagobj;
		
		endforeach;
	
	endif;

?>