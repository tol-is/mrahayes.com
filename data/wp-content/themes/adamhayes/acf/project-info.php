<?php

	$project_post = array();

	$project_post['id'] = $post->ID;
	$project_post['slug'] = $post->post_name;
	$project_post['title'] = $post->post_title;
	$project_post['info'] = get_field('project_copy');
	
	$project_post['bgColor'] = get_field('bg_color');
	$project_post['primaryColor'] = get_field('primary_color');
	$project_post['copyColor'] = get_field('copy_color');

	$content = array();
	
	while(the_flexible_field("project_content")):

		$content_row = array();
		
		$rowType = get_row_layout();
		
		$content_row['type'] = $rowType;

		switch ($rowType)
		{

			case 'project_image':
				$imgId = get_sub_field('project_image');
				
				$wpimage = wp_get_attachment_image_src($imgId, "full");
				$img_1200 = wp_get_attachment_image_src($imgId, "img_1200");
				$img_900 = wp_get_attachment_image_src($imgId, "img_900");
				$img_700 = wp_get_attachment_image_src($imgId, "img_700");
				$img_400 = wp_get_attachment_image_src($imgId, "img_400");

				$image["img_1200"] = str_replace("http://adamhayes.local/", "/", $img_1200[0] ) ;
				$image["img_900"] = str_replace("http://adamhayes.local/", "/", $img_900[0] ) ;
				$image["img_700"] = str_replace("http://adamhayes.local/", "/", $img_700[0] ) ;
				$image["img_400"] = str_replace("http://adamhayes.local/", "/", $img_400[0] ) ;

				$image["width"] = $wpimage[1];
				$image["height"] = $wpimage[2];
				$content_row["image"] = $image;
			break;

			case 'project_video':
				$content_row["embed_code"] = get_sub_field('video_embed_code');
				$content_row["aspect_ratio"] = get_sub_field('video_ratio');
			break;

		}

		$content[] = $content_row;

	endwhile;

	$project_post['content'] = $content;

