<?php
/*
Template Name: Projects Page
*/

get_header();

	// If we have projects
	if (have_posts()) : 

		$works = array();

		while (have_posts()) : the_post();

			include("acf/project-preview.php");
			$works[] = $row_post;

    	endwhile;
		
		$response["works"] = $works;

    endif;

    // ech as json
    echo json_encode($response);

?>