<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */


// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'adamhayes_db');
//define('DB_NAME', 'adam_app');

/** MySQL database username */
define('DB_USER', 'root');
//define('DB_USER', 'adam_app');

/** MySQL database password */
define('DB_PASSWORD', 'root');
//define('DB_PASSWORD', '@d@mh@y3s2013');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');
//define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/*
define('FS_METHOD', 'ftpext');
define('FTP_BASE', '/var/www/vhosts/mrahayes.com/httpdocs/data/');
define('FTP_USER', 'adam');
define('FTP_PASS', '@d@mh@y3s2013');
define('FTP_HOST', 'ftp.enjoythisyeah.com'); 
define('FTP_SSL', false);
*/

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ':zIbB8)#[l6EfO2b6Hj3?5SsLG}9d+}K4ntdXwNVGc?&pOLp|u<l&7`r-r?J3]<}');
define('SECURE_AUTH_KEY',  'fMAFrOWvSO-UljZ{:I;t/%5s)I)09[$tvPQmh=w:qa1kVqJ98kD96ft)Wyg&:pz+');
define('LOGGED_IN_KEY',    'XZ 3cr{<f1xo3Jiy6)B)_#E@;ji:yetqnRXb=O|K=_nY$c`9@9>++Pfh>*7v=).W');
define('NONCE_KEY',        'R*C@u(g/i-*W+W<:~`EHf4:bN?[xl^74j1Vepve!:2U| 3cmghi!v8DwS)d0Ec|f');
define('AUTH_SALT',        'zz]TB;;IVKf}| _4rRP)~p{El6;Cx]Hfc@N$: 2@YvFZfMyN@92g=5@arp,R+w0|');
define('SECURE_AUTH_SALT', '-+HT|dw:s|R@Oi0Y,BmE<pq=X K<%,s+d+ZV-]^:aDe)j$/~#7UKU2srC?OwkP6{');
define('LOGGED_IN_SALT',   '^f#vaSXa$eGS]mAX?rKI)+-_oLJfZ|<<ZBo1j;Y5;ET>3yN/Ll<a6-]+#6h+G&9)');
define('NONCE_SALT',       'U[rZ&(v-JY-}F6B-xfT0nm}fr`er)R%P*U,06N~}n~sVN3ZtwEm3y ?<_l2Y6{a9');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ah_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
