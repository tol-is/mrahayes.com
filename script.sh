echo "Deleting all .DS_Store files";
find . -name .DS_Store | xargs rm -rf;
echo "Toasting";
toaster -w;